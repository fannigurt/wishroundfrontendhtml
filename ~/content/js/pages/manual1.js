function footerMarginCookie() {
	var cookieHeight = $(".cookie").height();
	var footer = $("footer");

	if (!$(".cookie").hasClass("is-closed")) {
		footer.css("padding-bottom", cookieHeight);
		// footer.css('transform',"translate3d(0px, " + cookieHeight + "px, 0px)");
	}
}

function footerMarginClear() {
	$("footer").css("padding-bottom", "0");
}


$(document).ready(function() {

	footerMarginCookie();

	$(window).resize(function () {
		footerMarginCookie();
	});

	//-refactoring scroll
	function scrollIndex() {
		if ($(window).width() > 980) {
			smoothScroll.init({
				offset: 60
			});
		} else {
			smoothScroll.init({
				offset: 0
			});
		}
	}
	scrollIndex();

	$(function() {
		$('.cookie-succes').on('click', function() {
			$(".cookie").slideUp();

			$(".cookie").addClass("is-closed");
			footerMarginClear();

			return false;
		});
	});

	function updateText(event){
		var input=$(this);
		setTimeout(function(){
			var val=input.val();
			if(val!="")
				input.parent().addClass("floating-placeholder-float");
			else if (!$(".floating-placeholder").hasClass("m-link"))
				input.parent().removeClass("floating-placeholder-float");
			// else
			// 	input.parent().addClass("floating-placeholder-float");
		},100)
	}
	$(".floating-placeholder input, .floating-placeholder textarea").keydown(updateText);
	$(".floating-placeholder input, .floating-placeholder textarea").change(updateText);

	function getLinks(text) {
		var expression = /((https?:\/\/)[\w-]+(\.[\w-]+)+\.?(:\d+)?(\/\S*)?)/gi;
		return (text.match(expression));
	}

	function checkRemovePlacehodler(input) {
		var id = input.attr("id");
		var val = '';
		if (id === "price") {
			input.attr('type', 'text');
			val = input.val();
			var regex = /[^0-9.,]/g;
			if (regex.test(val)) {
				val = val.replace(regex, '');
				input.val(val);
			}
			input.attr('type', 'number');
		}
		val = input.val();
		if (val.length > 0) {
			input.parent().addClass("floating-placeholder-float");
		} else {
			input.parent().removeClass("floating-placeholder-float");
		}
	}

	$('.js-validateform .input input').each(function () {
		var input = $(this);
		checkRemovePlacehodler(input);
	});

	var linkInputWaiting = 0;
	var linkInputLastChecked = '';
	var linkCheckXhr = null;

	function onInputChange(e) {
		var input = $(this);
		setTimeout(function () {
			checkRemovePlacehodler(input);
		}, 100);
	}

	function getParentFormRow(input) {
		var pI = input.parent();
		while (pI != null && !pI.hasClass("form-row")) {
			pI = pI.parent();
		}
		return pI;
	}

	function startPreloaderOnInput(input) {
		var pI = getParentFormRow(input);
		if (pI == null) return;
		if (!pI.hasClass("is-load")) {
			pI.addClass("is-load");
			input.attr("disabled", true);
		}
	}

	function removerPreloaderFromInput(input) {
		var pI = getParentFormRow(input);
		if (pI.hasClass("is-load")) {
			pI.removeClass("is-load");
			input.removeAttr("disabled");
		}
	}

	function onInputKeydown(event) {
		var input = $(this);
		var id = input.attr("id");
		if (id === "price") {
			if (event.type === "keydown"
					&& !(event.keyCode === 8 // backspace
					|| event.keyCode === 46 // delete
					|| (event.keyCode >= 35 && event.keyCode <= 40) // arrow keys/home/end
					|| (event.keyCode >= 48 && event.keyCode <= 57) // numbers on keyboard
					|| (event.keyCode >= 96 && event.keyCode <= 105) // number on keypad
					|| (event.keyCode === 188 || event.keyCode === 190 || event.keyCode === 110)) // dot, comma and numeric keypad dot
			) {
				event.preventDefault(); // Prevent character input
			} else {
				setTimeout(function () {
					checkRemovePlacehodler(input);
				}, 100);
			}
		} else if (id === "link") {
			setTimeout(function () {
				checkRemovePlacehodler(input);

				linkInputWaiting++;
				setTimeout(function () {
					linkInputWaiting--;
					if (linkInputWaiting === 0) {
						var links = getLinks(input.val());
						if (links != null && links.length > 0) {
							if (linkInputLastChecked !== links[0]) {
								linkInputLastChecked = links[0];

								startPreloaderOnInput($("#link"));
								startPreloaderOnInput($("#price"));

								if (linkCheckXhr !== null) {
									linkCheckXhr.abort();
								}

								var d = {
									wishId: WISH_ID,
									link: links[0]
								};

								linkCheckXhr = $.ajax({
									type: "POST",
									url: BASE_URL + 'Ajax/CmProcessLink',
									contentType: "application/json; charset=utf-8",
									data: JSON.stringify(d),
									dataType: "json",
									success: function (response) {
										if (response !== null && response !== undefined) {
											if (response.Success === true) {
												if (response.Title != null) {
													$('#link').val(response.Title);
												}
												if (response.Price != null) {
													var price = parseFloat(response.Price);
													if (price > 0) {
														$('#price').val(price);
														checkRemovePlacehodler($('#price'));
													} else {
														$('#price').val('');
														checkRemovePlacehodler($('#price'));
													}
												} else {
													$('#price').val('');
													checkRemovePlacehodler($('#price'));
												}
											} else {
												console.log(response);
											}
										}
									},
									complete: function () {
										removerPreloaderFromInput($("#link"));
										removerPreloaderFromInput($("#price"));
									}
								});
							}
						}
					}
				}, 1500);
			}, 100);
		} else {
			setTimeout(function () {
				checkRemovePlacehodler(input);
			}, 100);
		}
	}

	function popupWindow(url, title, w, h) {
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

		var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		var newWindow = window.open(url, title, 'directories=no, status=no, resizable=no, copyhistory=no, scrollbars=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
		return newWindow;
	}

	$(".floating-placeholder input, .floating-placeholder textarea").on('paste', onInputKeydown);
	$(".floating-placeholder input, .floating-placeholder textarea").on('keydown', onInputKeydown);
	$(".floating-placeholder input, .floating-placeholder textarea").on('change', onInputChange);

	var fillNameFacebookXhr = null;
	var facebookLoginPopup = null;
	var facebookLoginPopupTimer = null;

	function checkFacebookPopupWindow() {
		if (facebookLoginPopup == null || facebookLoginPopup.closed === true) {
			clearInterval(facebookLoginPopupTimer);
			removerPreloaderFromInput($("#name"));
		}
	}

	function checkFacebookPopupCallback(data) {
		if (data) {
			if (data.Success === true) {
				if (data.UserName) {
					var input = $('#name');
					input.val(data.UserName);
					checkRemovePlacehodler(input);
				}
			}
		}
		facebookLoginPopup.close();
		removerPreloaderFromInput($("#name"));
	}

	if (!window.checkFacebookPopupCallback) {
		window.checkFacebookPopupCallback = checkFacebookPopupCallback;
	}

	$('#fillNameFacebook').click(function () {
		startPreloaderOnInput($("#name"));

		if (fillNameFacebookXhr !== null) {
			fillNameFacebookXhr.abort();
		}

		var d = {};

		if (facebookLoginPopup === null || facebookLoginPopup === undefined || facebookLoginPopup.closed === true) {
			if (facebookLoginPopupTimer != null) {
				clearInterval(facebookLoginPopupTimer);
				facebookLoginPopupTimer = null;
			}
			facebookLoginPopupTimer = setInterval(checkFacebookPopupWindow, 500);
			facebookLoginPopup = popupWindow(BASE_URL + 'System/PopupWait', "_blank", 700, 500);
			facebookLoginPopup.focus();
		} else {
			// TODO: ???
		}

		fillNameFacebookXhr = $.ajax({
			type: "POST",
			url: BASE_URL + 'Ajax/CmFillNameFacebook',
			contentType: "application/json; charset=utf-8",
			data: JSON.stringify(d),
			dataType: "json",
			success: function (response) {
				if (response !== null && response !== undefined) {
					if (response.Success === true) {
						if (response.LoginPopupUrl !== null && response.LoginPopupUrl !== undefined) {
							if (facebookLoginPopup === null || facebookLoginPopup === undefined || facebookLoginPopup.closed === true) {
								removerPreloaderFromInput($("#name"));
								return;
							}
							facebookLoginPopup.location = response.LoginPopupUrl;
							if (window.focus) {
								facebookLoginPopup.focus();
							}
						} else {
							removerPreloaderFromInput($("#name"));
							if (facebookLoginPopup !== null) facebookLoginPopup.close();
						}
					} else {
						removerPreloaderFromInput($("#name"));
						if (facebookLoginPopup !== null) facebookLoginPopup.close();
						console.log(response);
					}
				}
			},
			error: function (err) {
				removerPreloaderFromInput($("#name"));
				if (facebookLoginPopup !== null) facebookLoginPopup.close();
				console.log(err);
			}
		});

		return false;
	});

	// datepicker
	$(function(){
		$('.js-datepicker').datepicker({
			minDate: 0,
			showOtherMonths: true,
			selectOtherMonths: false
		});
	});


  // Set form select active element
  if ($('select.cs-select option[selected]').length > 0) {
    var els = $('select.cs-select option');
    var idx = 0;
    for (var i = 0; i < els.length; i++) {
      if (els[i].hasAttribute('selected')) {
        idx = i;
        break;
      }
    }
    $(".form .cs-options li").eq(idx).addClass("cs-selected");
  } else {
	 	$(".form .cs-options li").first().addClass("cs-selected");
	}

	// tooltip error
	$( ".js-tooltip-error" ).hover(
		function() {
			$(".js-tooltip-badlink").stop();
			$(".js-tooltip-badlink").fadeIn(400);
		}, function() {
			$(".js-tooltip-badlink").stop();
		 $(".js-tooltip-badlink").fadeOut(400);
		}
	);

	// footer lang
	$('.footer-lang').on('click', function() {
		$(this).toggleClass("is-click");
		$(".lang-tooltip").slideToggle("fast");
	});
	$(document).on('click', function (e) {
		if ($(e.target).closest(".footer-lang").length === 0) {
			$(".footer-lang").removeClass("is-click");
			$(".lang-tooltip").slideUp("fast");
		}
	});

	// tooltip hover
	$( ".js-tooltip" ).hover(
		function() {
			$(".tooltip-open").stop();
			$(".tooltip-open").fadeIn(400);
			$(this).addClass("is-hover");
		}, function() {
			$(".tooltip-open").stop();
			$(".tooltip-open").fadeOut(400);
			$(this).removeClass("is-hover");
		}
	);

	// switcher
	$('.js-switchoption .switchoption-item').on('click', function() {
		if ($(this).attr("id") === "friend" ) {
			$(".js-validateform").addClass("is-switch");
		}

		if ($(this).attr("id") === "for-me" ) {
			$(".js-validateform").removeClass("is-switch");
		}
	});

	// if ($(".tooltip-error").hasClass("is-no-valid")) {
	// 	$(".tooltip-error").fadeIn().delay(2800).fadeOut();
	// }

	function isBlankString(str) {
		return (!str || /^\s*$/.test(str));
	}

	function bounceInput(input) {
		if (!input.parent().hasClass("wobble animated")) {
			input.parent().addClass("wobble animated");

			setTimeout(function () {
				input.parent().removeClass("wobble animated");
			}, 900);
		}
	}

	var sendFormXhr = null;

	// validate form
	$('.js-sendform').on('click', function() {

		if (sendFormXhr != null) return false;

		var dueDate = $("#datepicker").datepicker("getDate");
		if (dueDate != null) {
			var dd = dueDate.getDate();
			var mm = dueDate.getMonth() + 1;
			var yyyy = dueDate.getFullYear();
			if (dd < 10) {
				dd = '0' + dd;
			}
			if (mm < 10) {
				mm = '0' + mm;
			}
			dueDate = dd + '.' + mm + '.' + yyyy;
		}
		
		var d = {
			wishId: WISH_ID,
			anniversaryType: $('#anniversaryType').val(),
			title: $('#link').val(),
			name: $('#name').val(),
			amount: $('#price').val(),
			dueDate: dueDate
		};

		var err = false;
		if (isBlankString(d.anniversaryType)) {
			bounceInput($('#anniversaryType'));
			err = true;
		}
		if (isBlankString(d.title)) {
			bounceInput($('#link'));
			err = true;
		}
		if (isBlankString(d.name)) {
			bounceInput($('#name'));
			err = true;
		}
		if (isBlankString(d.amount)) {
			bounceInput($('#price'));
			err = true;
		}
		if (isBlankString(d.dueDate)) {
			bounceInput($('#datepicker'));
			err = true;
		}

		if (!err) {
			startPreloaderOnInput($('#btn-form-send'));
			sendFormXhr = $.ajax({
				type: "POST",
				url: BASE_URL + 'Ajax/CmStep1Post',
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(d),
				dataType: "json",
				success: function (response) {
					if (response !== null && response !== undefined) {
						if (response.Success === true) {
							window.location.reload(true);
						} else {
							removerPreloaderFromInput($('#btn-form-send'));
							console.log(response);
						}
					} else {
						removerPreloaderFromInput($('#btn-form-send'));
					}
				},
				complete: function () {
					sendFormXhr = null;
				}
			});
		}
		return false;
	});

	function slick4() {
		$('.js-user').slick({
			dots: true,
			infinite: true,
			arrows: false,
			slidesToShow: 2,
			slidesToScroll: 2,
			adaptiveHeight: true,
			responsive: [
		{
			breakpoint: 960,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			}
		},
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		},
			]
		});
	}
	if ($(".js-user").length > 0) {
		slick4();
	}

});
