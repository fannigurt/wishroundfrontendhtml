window.onload = init;

function init () {
	initFrame ();
}

function createIframe () {

	//
	var myOverlay = document.createElement('div');
	myOverlay.setAttribute("id", "wishroundIframeOverlay");
	myOverlay.setAttribute('style', 'display:block !important; width:100% !important; min-width:1200px !important; height:100% !important; z-index:9999 !important; position:absolute !important; top:0px !important; left:0px !important; border-width:0px !important; background:rgba(255, 255, 255, 0.78) !important; margin:0px !important; padding:0px !important; overflow-x:hidden !important; overflow-y:visible !important;');
	
	//
	var wishroundWrap = document.createElement('div');
	wishroundWrap.setAttribute("id", "wishroundIframeWrap");
	wishroundWrap.setAttribute('style', 'width: 600px !important;position: absolute !important; left: 50% !important; margin-left: -300px !important; top: 90px !important;text-align:center !important;');

	//
	var ifr = document.createElement('iframe');
	ifr.src = 'http://wishroundhtml.azurewebsites.net/mangopay_payment_cashout.html';
	ifr.setAttribute("id", "wishroundFrame");
	ifr.setAttribute('style', 'height: 650px; !important;width: 600px !important;background-color: rgba(255, 255, 255, 1) !important;background: rgba(255, 255, 255, 1) !important;box-shadow: 0 0 80px rgba(135, 155, 182, 0.4) !important;border: 0 !important; margin-bottom: 100px !important;');

	// document.body.style.overflow = "hidden";
	document.body.style.position = "relative";
	document.body.style.height = "auto";
	document.body.setAttribute("class", "header-absolute m-desktop");

	wishroundWrap.appendChild(ifr);
	myOverlay.appendChild(wishroundWrap);
	document.body.appendChild(myOverlay);

}

function receiveMessage(event){
  if (event.data=="removetheiframe"){
    var element = document.getElementById('wishroundFrame');
    var myOverlay = document.getElementById('wishroundIframeOverlay');
    var wishroundWrap = document.getElementById("wishroundIframeWrap");

    element.parentNode.removeChild(element);
    myOverlay.parentNode.removeChild(myOverlay);
    wishroundWrap.parentNode.removeChild(wishroundWrap);

    // document.body.style.overflow = "";
    document.body.style.position = "";
    document.body.style.height = "100%";
    document.body.setAttribute("class", "m-desktop");
  }
  // TODO: remove EventListender
}

function initFrame () {
	document.getElementById('btn-chip-in').onclick = function() {
		createIframe ();
		window.addEventListener("message", receiveMessage, false);
	}
}

function openPopupCloseWish() {
	$("#close-wish").addClass("md-show");

	$("body").addClass("is-popup");
	$("html").addClass("is-popup");
}

function initPopupHeight() {
	// var bodyHeight = $("body").height();
	var bodyHeight = screen.height;
	$(".md-modal .md-overlay").css("min-height", bodyHeight);
}

function initClosePopup() {
	$('.md-close').on('click', function() {
		$("body").removeClass("is-popup");
		$("html").removeClass("is-popup");
		$(".md-modal").removeClass("md-show");
		$(".nav-mobile").removeClass("is-active");
	});
	$('.md-overlay').on('click', function() {
		$(".md-modal").removeClass("md-show");
		$("body").removeClass("is-popup");
		$("html").removeClass("is-popup");
		$(".nav-mobile").removeClass("is-active");
	});
}

function initPopupCloseWish() {
	var link = $("a[data-modal='close-wish']"); 

	link.on('click', function() {
		if ($(this).hasClass("md-trigger")) {
			openPopupCloseWish();
			return false;
		}
	});
}

// function initPopupAskMeLater() {
// 	var link = $(".js-ask-later");

// 	link.on('click', function() {
// 		$("#close-wish").removeClass("md-show");
// 		$("body").removeClass("is-popup");
// 		$("html").removeClass("is-popup");

// 		$(".js-wishinfo").addClass("is-active");
// 		$(".wishoverlay").addClass("is-active");
// 		$(".wish").addClass("is-overlay");


// 		// return false;
// 	});
// }

function msieversion() {
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf("MSIE ");

  if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
  	  console.log(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
  		$("body").addClass("ie");
  }
  else {
  	// console.log('otherbrowser');
  }
  return false;
}


$(document).ready(function () {

	initPopupHeight();
	initClosePopup();

	initPopupCloseWish();
	// initPopupAskMeLater();

	msieversion();

	// footer lang
	$('.footer-lang').on('click', function() {
		$(this).toggleClass("is-click");
		$(".lang-tooltip").slideToggle("fast");
	});
	$(document).on('click', function (e) {
		if ($(e.target).closest(".footer-lang").length === 0) {
			$(".footer-lang").removeClass("is-click");
			$(".lang-tooltip").slideUp("fast");
		}
	});

	// open user nav
	$('.js-user-login').on('click', function() {
		$(this).parents("header").find(".user-login").toggleClass("is-open");
		$(this).parents("header").find(".user-subnav").toggleClass("is-open");
		$(".user-overlay").toggleClass("is-active");
		$(".user-subnav").slideToggle(200);
		return false;
	});
	// close user nav
	$(document).on('click', function (e) {
		if ($(e.target).closest(".user-login").length === 0) {
			$(".user-login").removeClass("is-open");
			$(".user-subnav").removeClass("is-open");
			$(".user-overlay").removeClass("is-active");
			$(".user-subnav").slideUp(200);
		}
	});

	//
	$('.js-add-day').on('click', function() {
		$(".wish").removeClass("is-overlay");
		$(".wishinfo").removeClass("is-active");
		$(".wishoverlay").removeClass("is-active");
		return false;
	});

	//
	$('#js-congrats').on('click', function() {
		$("#get-form").addClass("is-active");
		$("#js-congrats").removeClass("is-active");
		return false;
	});



	// validate form 
	$('#js-merchform-connect').on('click', function () {
    $('#wishform input').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $("#wishform");

			// validation input
			if (val != "") {
				input.addClass("is-succes");
				input.removeClass("is-error");
			} else {
					input.addClass("is-error");
					input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
					input.addClass("bounce animated");

					var delay = setTimeout(function () {
						input.removeClass("bounce animated");
					}, 800)
			} else {
				input.removeClass("bounce animated");
			}


		});

		// validation form
		if (!$('#wishform input').hasClass("is-error")) {
			$("#wishform").addClass("is-succes");
			$("#wishform").removeClass("is-error");
		} else {
			$("#wishform").addClass("is-error");
			$("#wishform").removeClass("is-succes");
		} 

		// open #connect-success popup
		if ($("#wishform").hasClass("is-succes")) {
			$("#get-form").removeClass("is-active");
			$("#thank").addClass("is-active");
		}
		return false;
	});
	
	
});