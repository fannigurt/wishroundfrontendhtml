window.onload = init;

function init () {
	initFrame ();
}

function openPopupCountry() {
	$("#country").addClass("md-show");

	$("body").addClass("is-popup");
	$("html").addClass("is-popup");
}
function initPopupHeight() {
	// var bodyHeight = $("body").height();
	var bodyHeight = screen.height;
	$(".md-modal .md-overlay").css("min-height", bodyHeight);
}

function initClosePopup() {
	$('.md-close').on('click', function() {
		$("body").removeClass("is-popup");
		$("html").removeClass("is-popup");
		$(".md-modal").removeClass("md-show");
		$(".nav-mobile").removeClass("is-active");
	});
	$('.md-overlay').on('click', function() {
		$(".md-modal").removeClass("md-show");
		$("body").removeClass("is-popup");
		$("html").removeClass("is-popup");
		$(".nav-mobile").removeClass("is-active");
	});
}
function initPopupCountry() {
	openPopupCountry(); 
}

function createIframe () {

	//
	var myOverlay = document.createElement('div');
	myOverlay.setAttribute("id", "wishroundIframeOverlay");
	myOverlay.setAttribute('style', 'display:block !important; width:100% !important; min-width:1200px !important; height:100% !important; z-index:9999 !important; position:absolute !important; top:0px !important; left:0px !important; border-width:0px !important; background:rgba(255, 255, 255, 0.78) !important; margin:0px !important; padding:0px !important; overflow-x:hidden !important; overflow-y:visible !important;');
	
	//
	var wishroundWrap = document.createElement('div');
	wishroundWrap.setAttribute("id", "wishroundIframeWrap");
	wishroundWrap.setAttribute('style', 'width: 600px !important;position: absolute !important; left: 50% !important; margin-left: -300px !important; top: 90px !important;text-align:center !important;');

	//
	var ifr = document.createElement('iframe');
	ifr.src = 'http://wishroundhtml.azurewebsites.net/mangopay_payment.html';
	ifr.setAttribute("id", "wishroundFrame");
	ifr.setAttribute('style', 'height: 650px; !important;width: 600px !important;background-color: rgba(255, 255, 255, 1) !important;background: rgba(255, 255, 255, 1) !important;box-shadow: 0 0 80px rgba(135, 155, 182, 0.4) !important;border: 0 !important; margin-bottom: 100px !important;');

	document.body.style.position = "relative";
	// document.body.style.overflow = "hidden";
	document.body.style.height = "auto";
	document.body.setAttribute("class", "header-absolute");

	wishroundWrap.appendChild(ifr);
	myOverlay.appendChild(wishroundWrap);
	document.body.appendChild(myOverlay);

}

function receiveMessage(event){
  if (event.data=="removetheiframe"){
    var element = document.getElementById('wishroundFrame');
    var myOverlay = document.getElementById('wishroundIframeOverlay');
    var wishroundWrap = document.getElementById("wishroundIframeWrap");

    element.parentNode.removeChild(element);
    myOverlay.parentNode.removeChild(myOverlay);
    wishroundWrap.parentNode.removeChild(wishroundWrap);

    document.body.style.position = "";
    // document.body.style.overflow = "";
    document.body.style.height = "";
    document.body.setAttribute("class", "");
  }
  // TODO: remove EventListender
}

function initFrame () {
	document.getElementById('btn-chip-in').onclick = function() {

		if ($(".js-step-form").hasClass("is-succes")) {
			createIframe ();
		}

		window.addEventListener("message", receiveMessage, false);
	}
}

function footerMarginCookie() {
	var cookieHeight = $(".cookie").height();
	var footer = $("footer");

	if (!$(".cookie").hasClass("is-closed")) {
		footer.css("padding-bottom", cookieHeight);
		// footer.css('transform',"translate3d(0px, " + cookieHeight + "px, 0px)");
	}
}

function footerMarginClear() {
	$("footer").css("padding-bottom", "0");
}

$(document).ready(function () {

	initPopupHeight();
	initClosePopup();
	// initPopupMobile();

	// initPopupCountry();

	footerMarginCookie();

	$(window).resize(function () {
		footerMarginCookie();
	});

	// validate form
	$('#btn-chip-in').on('click', function () {
		$('.js-form-required').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $(".js-step-form");

			// validation input
			if (val != "") {
				input.addClass("is-succes");
				input.removeClass("is-error");
			} else {
				input.addClass("is-error");
				input.removeClass("is-succes");

				// for anonymous state
				if (input.hasClass("is-disabled")) {
					$("#name").addClass("is-succes");
					$("#fio").addClass("is-succes");
					$("#name").removeClass("is-error");
					$("#fio").removeClass("is-error");
				}
			}

			// input animation
			if (input.hasClass("is-error")) {
				input.parent().addClass("wobble animated");

				var delay = setTimeout(function () {
						input.parent().removeClass("wobble animated");
				}, 800)
			} else {
				input.parent().removeClass("wobble animated");
			}
		});

		var input = $(".js-form-required");
		var val = input.val();
		var form = $(".js-step-form");

		// validation form
		if (!$(input).hasClass("is-error")) {
			form.addClass("is-succes");
			form.removeClass("is-error");
		} else {
			form.addClass("is-error");
			form.removeClass("is-succes");
		}
	});

	// anonymously
	$('.js-anonymously :checkbox').click(function() {
    var $this = $(this);

    if ($this.is(':checked')) {
      $("#name").addClass("is-disabled");
      $("#name").attr('disabled',true);
      $("#fio").addClass("is-disabled");
      $("#fio").attr('disabled',true);

      // reaquired anonnimus uncheck
      // $("#name").addClass("is-succes");
      // $("#fio").addClass("is-succes");
    } else {
      $("#name").removeClass("is-disabled");
      $("#name").removeAttr('disabled');
      $("#fio").removeClass("is-disabled");
      $("#fio").removeAttr('disabled');
    }
	});

	// tooltip error
	$( ".js-tooltip-error" ).hover(
		function() {
			$(".tooltip-error").stop();
			$(".tooltip-error").fadeIn(400);
		}, function() {
			$(".tooltip-error").stop();
		 $(".tooltip-error").fadeOut(400);
		}
	);

	// footer lang
	$('.footer-lang').on('click', function() {
		$(this).toggleClass("is-click");
		$(".lang-tooltip").slideToggle("fast");
	});
	$(document).on('click', function (e) {
		if ($(e.target).closest(".footer-lang").length === 0) {
			$(".footer-lang").removeClass("is-click");
			$(".lang-tooltip").slideUp("fast");
		}
	});

	// tooltip hover
	$( ".js-tooltip" ).hover(
		function() {
			$(".tooltip-open").stop();
			$(".tooltip-open").fadeIn(400);
			$(this).addClass("is-hover");
		}, function() {
			$(".tooltip-open").stop();
			$(".tooltip-open").fadeOut(400);
			$(this).removeClass("is-hover");
		}
	);

	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		var expires = "expires=" + d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	}

	$(function () {
		$('.cookie-succes').on('click', function () {
			setCookie("WishroundCA", "1", 365);
			$(".cookie").slideUp();

			$(".cookie").addClass("is-closed");
			footerMarginClear();

			return false;
		});
	});

	// open user nav
	$('.js-user-login').on('click', function() {
		$(this).parents("header").find(".user-login").toggleClass("is-open");
		$(this).parents("header").find(".user-subnav").toggleClass("is-open");
		$(".user-overlay").toggleClass("is-active");
		$(".user-subnav").slideToggle(200);
		return false;
	});
	// close user nav
	$(document).on('click', function (e) {
		if ($(e.target).closest(".user-login").length === 0) {
			$(".user-login").removeClass("is-open");
			$(".user-subnav").removeClass("is-open");
			$(".user-overlay").removeClass("is-active");
			$(".user-subnav").slideUp(200);
		}
	});

	$('.slider-for').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  fade: true,
	  infinite: false,
	  asNavFor: '.slider-nav'
	});

	$('.slider-nav').slick({
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  asNavFor: '.slider-for',
	  dots: false,
	  focusOnSelect: true,
	  centerMode: false,
	  infinite: false,
	  prevArrow: ".wish-caro-prev",
	  nextArrow: ".wish-caro-next",
	  onAfterChange:function(slickSlider,i){
      //remove all active class
      $('.slider-nav .slick-slide').removeClass('is-active');
      //set active class for current slide
      $('.slider-nav .slick-slide').eq(i).addClass('is-active');         
	   }
	});

	function updateText(event){
		var input=$(this);
		setTimeout(function(){
			var val=input.val();
			if(val!="")
				input.parent().addClass("floating-placeholder-float");
			else if (!$(".floating-placeholder").hasClass("m-link"))
				input.parent().removeClass("floating-placeholder-float");
			// else
			// 	input.parent().addClass("floating-placeholder-float");
		},100)
	}
	$(".floating-placeholder input, .floating-placeholder textarea").keydown(updateText);
	$(".floating-placeholder input, .floating-placeholder textarea").change(updateText);

	// ДОПИЛ
	$('.js-step-summ').keyup(function(eventObject){
		var myValue = this.value;
	  // console.log(myValue);
	});

	// focus step-summ
	swapValue = [];
	$(".step-summ-input").each(function(i){
	  swapValue[i] = $(this).val();
	  $(this).focus(function(){
      if ($(this).val() == swapValue[i]) {
        $(this).val("");
      }
	    $(this).parent().addClass("focus");
	  }).blur(function(){
	    if ($.trim($(this).val()) == "") {
        $(this).val(swapValue[i]);
	 			$(this).parent().removeClass("focus");
	    }
	   });
	});

});