window.onload = init;

function init () {
	initFrame ();
}

function createIframe () {

	//
	var myOverlay = document.createElement('div');
	myOverlay.setAttribute("id", "wishroundIframeOverlay");
	myOverlay.setAttribute('style', 'display:block !important; width:100% !important; min-width:1200px !important; height:100% !important; z-index:9999 !important; position:absolute !important; top:0px !important; left:0px !important; border-width:0px !important; background:rgba(255, 255, 255, 0.78) !important; margin:0px !important; padding:0px !important; overflow-x:hidden !important; overflow-y:visible !important;');
	
	//
	var wishroundWrap = document.createElement('div');
	wishroundWrap.setAttribute("id", "wishroundIframeWrap");
	wishroundWrap.setAttribute('style', 'width: 458px !important;position: absolute !important; left: 50% !important; margin-left: -229px !important; top: 90px !important;text-align:center !important;');

	//
	var wishroundLink = document.createElement('a');
	wishroundLink.innerHTML = "What's is Wishround?";
	wishroundLink.setAttribute("id", "wishroundIframeLink");
	wishroundLink.setAttribute("href", "google.com");
	wishroundLink.setAttribute('style', 'font-size: 12px !important;color:#4a94f9 !important;text-decoration: none !important;font-family: "Open Sans", sans-serif !important;font-weight:300 !important;margin: 20px auto !important;display: inline-block !important;');

	//
	var ifr = document.createElement('iframe');  
	// ifr.src = 'http://localhost:8080/checkout_popup.html';
	ifr.src = 'http://wishroundhtml.azurewebsites.net/checkout_popup.html';
	ifr.setAttribute("id", "wishroundFrame");
	ifr.setAttribute('style', 'height: 395px; !important;width: 458px !important;background-color: rgba(255, 255, 255, 0.78) !important;background: rgba(255, 255, 255, 0.78) !important;box-shadow: 0 0 80px rgba(135, 155, 182, 0.4) !important;border: 0 !important;');

	document.body.style.position = "relative";

	wishroundWrap.appendChild(ifr);
	wishroundWrap.appendChild(wishroundLink);
	myOverlay.appendChild(wishroundWrap);
	document.body.appendChild(myOverlay);
}

function receiveMessage(event){
  if (event.data=="removetheiframe"){
    var element = document.getElementById('wishroundFrame');
    var myOverlay = document.getElementById('wishroundIframeOverlay');
    var wishroundWrap = document.getElementById("wishroundIframeWrap");
    var wishroundLink = document.getElementById("wishroundIframeLink");

    element.parentNode.removeChild(element);
    myOverlay.parentNode.removeChild(myOverlay);
    wishroundWrap.parentNode.removeChild(wishroundWrap);
    wishroundLink.parentNode.removeChild(wishroundLink);

    document.body.style.position = "";
  }
  // TODO: remove EventListender
}

function initFrame () {
	document.getElementById('wishroundSendPopup').onclick = function() {
		createIframe ();

		window.addEventListener("message", receiveMessage, false);
	}
}
