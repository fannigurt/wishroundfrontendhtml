function openPopupSignUp() {
	$("#sign-up").addClass("md-show");

	$("body").addClass("is-popup");
	$("html").addClass("is-popup");
}
function openPopupPreview() {
	$("#popup-preview").addClass("md-show");

	$("body").addClass("is-popup");
	$("html").addClass("is-popup");
}
function openPopupBirthday() {
	$("#birthday").addClass("md-show");

	$("body").addClass("is-popup");
	$("html").addClass("is-popup");
}

function initPopupHeight() {
	// var bodyHeight = $("body").height();
	var bodyHeight = screen.height;
	$(".md-modal .md-overlay").css("min-height", bodyHeight);
}
function initClosePopup() {
	$('.md-close').on('click', function () {
		$("body").removeClass("is-popup");
		$("html").removeClass("is-popup");
		$(".md-modal").removeClass("md-show");
		$(".nav-mobile").removeClass("is-active");
	});
	$('.md-overlay').on('click', function () {
		// skip birthday and email popup do disallow closing
		if ($('#birthday').hasClass('md-show') !== true) {
			$(".md-modal").removeClass("md-show");
			$("body").removeClass("is-popup");
			$("html").removeClass("is-popup");
			$(".nav-mobile").removeClass("is-active");
		}
	});
}
function initPopupSignUp() {
	$('.btn-next').on('click', function () {
		if ($(this).hasClass("md-trigger")) {
			openPopupSignUp();
			return false;
		}
	});
}
function initPopupPreview() {
	$('.js-popup-preview').on('click', function () {
		if ($(".js-popup-preview").hasClass("md-trigger")) {
			openPopupPreview();
			return false;
		}
	});
}

function footerMarginCookie() {
	var cookieHeight = $(".cookie").height();
	var footer = $("footer");

	if (!$(".cookie").hasClass("is-closed")) {
		footer.css("padding-bottom", cookieHeight);
		// footer.css('transform',"translate3d(0px, " + cookieHeight + "px, 0px)");
	}
}

function footerMarginClear() {
	$("footer").css("padding-bottom", "0");
}

$(document).ready(function () {

	initPopupHeight();
	initClosePopup();

	footerMarginCookie();

	$(window).resize(function () {
		footerMarginCookie();
	});

	//-refactoring scroll
	function scrollIndex() {
		if ($(window).width() > 980) {
			smoothScroll.init({
				offset: 60
			});
		} else {
			smoothScroll.init({
				offset: 0
			});
		}
	}
	scrollIndex();

	$(function() {
		$('.cookie-succes').on('click', function() {
			$(".cookie").slideUp();

			$(".cookie").addClass("is-closed");
			footerMarginClear();

			return false;
		});
	});

	function popupWindow(url, title, w, h) {
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

		var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		var newWindow = window.open(url, title, 'directories=no, status=no, resizable=no, copyhistory=no, scrollbars=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
		return newWindow;
	}

	function startPreloaderOnButton(button) {
		if (!button.hasClass("is-load")) button.addClass("is-load");
	}

	function stopPreloaderOnButton(button) {
		if (button.hasClass("is-load")) button.removeClass("is-load");
	}

	var backXhr = null;
	var nextXhr = null;
	var updEBXhr = null;

	var signUpFacebookXhr = null;
	var facebookLoginPopup = null;
	var facebookLoginPopupTimer = null;

	function checkFacebookPopupWindow() {
		if (facebookLoginPopup == null || facebookLoginPopup.closed === true) {
			clearInterval(facebookLoginPopupTimer);
			// TODO: Remove preloader from sign-up popup
		}
	}

	function checkFacebookPopupCallback(data) {
		if (data) {
			if (data.Success === true) {
				// TODO: Remove preloader from sign-up popup

				$("body").removeClass("is-popup");
				$("html").removeClass("is-popup");
				$(".md-modal").removeClass("md-show");
				$(".nav-mobile").removeClass("is-active");

				$('#btn-next').click();
			}
		}
		facebookLoginPopup.close();
		// TODO: Remove preloader from sign-up popup
	}

	if (!window.checkFacebookPopupCallback) {
		window.checkFacebookPopupCallback = checkFacebookPopupCallback;
	}

	$('#popup-sign-fb').on('click', function () {
		// TODO: Start preloader on sign-up popup

		if (signUpFacebookXhr !== null) {
			signUpFacebookXhr.abort();
		}

		var d = {};

		if (facebookLoginPopup === null || facebookLoginPopup === undefined || facebookLoginPopup.closed === true) {
			if (facebookLoginPopupTimer != null) {
				clearInterval(facebookLoginPopupTimer);
				facebookLoginPopupTimer = null;
			}
			facebookLoginPopupTimer = setInterval(checkFacebookPopupWindow, 500);
			facebookLoginPopup = popupWindow(BASE_URL + 'System/PopupWait', "_blank", 700, 500);
			facebookLoginPopup.focus();
		} else {
			console.log('err #1');
			// TODO: ???
		}

		signUpFacebookXhr = $.ajax({
			type: "POST",
			url: BASE_URL + 'Ajax/CmStep2SignUpFacebook',
			contentType: "application/json; charset=utf-8",
			data: JSON.stringify(d),
			dataType: "json",
			success: function (response) {
				if (response !== null && response !== undefined) {
					if (response.Success === true) {
						if (response.LoginPopupUrl !== null && response.LoginPopupUrl !== undefined) {

							if (facebookLoginPopup === null || facebookLoginPopup === undefined || facebookLoginPopup.closed === true) {
								// TODO: Remove preloader from sign-up popup
								return;
							}

							facebookLoginPopup.location = response.LoginPopupUrl;
							if (window.focus) {
								facebookLoginPopup.focus();
							}
						} else {
							// TODO: Remove preloader from sign-up popup
							if (facebookLoginPopup !== null) facebookLoginPopup.close();
						}
					} else {
						// TODO: Remove preloader from sign-up popup
						if (facebookLoginPopup !== null) facebookLoginPopup.close();
						console.log(response);
					}
				}
			},
			error: function (err) {
				// TODO: Remove preloader from sign-up popup
				if (facebookLoginPopup !== null) facebookLoginPopup.close();
				console.log(err);
			}
		});

		return false;
	});

	$('#btn-back').on('click', function () {

		if (backXhr != null || nextXhr != null) return false;

		var d = {
			wishId: WISH_ID
		};

		startPreloaderOnButton($('#btn-back'));
		backXhr = $.ajax({
			type: "POST",
			url: BASE_URL + 'Ajax/CmStep2Back',
			contentType: "application/json; charset=utf-8",
			data: JSON.stringify(d),
			dataType: "json",
			success: function (response) {
				if (response !== null && response !== undefined) {
					if (response.Success === true) {
						window.location.reload(true);
					} else {
						stopPreloaderOnButton($('#btn-back'));
						console.log(response);
					}
				} else {
					stopPreloaderOnButton($('#btn-back'));
				}
			},
			complete: function () {
				backXhr = null;
			}
		});
		return false;
	});

	function updateEmailAndBirthday(email, bDay) {
		if (updEBXhr != null) return false;

		var d = {
			email: email,
			birthday: bDay
		};

		var complete = false;

		// TODO: startPreloaderOnButton($('#btn-next'));

		updEBXhr = $.ajax({
			type: "POST",
			url: BASE_URL + 'Ajax/CmStep2UpdateEB',
			contentType: "application/json; charset=utf-8",
			data: JSON.stringify(d),
			dataType: "json",
			success: function (response) {
				if (response !== null && response !== undefined) {
					if (response.Success === true) {
						// TODO: Remove popup

						complete = true;
						$('#btn-next').click();

					} else {
						console.log(response);
					}
				}
			},
			complete: function () {
				updEBXhr = null;
				if (!complete) {
					// TODO: stopPreloaderOnButton($('#btn-next'));
				}
			}
		});
		return false;
	}

	$('#ebPopup-btn-send').on('click', function() {
		var date = null;
		var email = null;

		if ($('#ebPopup-input-birthday-wrapper').is(":visible")) {
			date = $('#ebPopup-input-birthday-day').val() + "." + $('#ebPopup-input-birthday-month').val() + "." + $('#ebPopup-input-birthday-year').val();
		}
		if ($('#ebPopup-input-email-wrapper').is(":visible")) {
			email = $('#ebPopup-input-email').val();
		}

		if (date !== null || email !== null) {
			// TODO: Check email validity
			updateEmailAndBirthday(email, date);
		}
	});

	$('#btn-next').on('click', function () {
		if (backXhr != null || nextXhr != null) return false;

		var d = {
			wishId: WISH_ID,
			creatorComment: $('#link4').val()
		};

		var complete = false;

		startPreloaderOnButton($('#btn-next'));
		nextXhr = $.ajax({
			type: "POST",
			url: BASE_URL + 'Ajax/CmStep2Post',
			contentType: "application/json; charset=utf-8",
			data: JSON.stringify(d),
			dataType: "json",
			success: function (response) {
				if (response !== null && response !== undefined) {
					if (response.Success === true) {
						if (response.NeedLogin === true) {
							openPopupSignUp();
						} else if (response.NeedBirthday === true || response.NeedEmail === true) {
							$('#ebPopup-title-needEb').hide();
							$('#ebPopup-title-needEmail').hide();
							$('#ebPopup-title-needBirthday').hide();

							$('#ebPopup-input-email-wrapper').hide();
							$('#ebPopup-input-birthday-title').hide();
							$('#ebPopup-input-birthday-wrapper').hide();

							if (response.NeedBirthday === true && response.NeedEmail === true) {
								$('#ebPopup-title-needEb').show();
								$('#ebPopup-input-email-wrapper').show();
								$('#ebPopup-input-birthday-title').show();
								$('#ebPopup-input-birthday-wrapper').show();
							} else if (response.NeedEmail === true) {
								$('#ebPopup-title-needEmail').show();
								$('#ebPopup-input-email-wrapper').show();
							} else if (response.NeedBirthday === true) {
								$('#ebPopup-title-needBirthday').show();
								$('#ebPopup-input-birthday-title').show();
								$('#ebPopup-input-birthday-wrapper').show();
							}

							openPopupBirthday();
						} else {
							complete = true;
							window.location.reload(true);
						}
					} else {
						console.log(response);
					}
				}
			},
			complete: function () {
				nextXhr = null;
				if (!complete) stopPreloaderOnButton($('#btn-next'));
			}
		});
		return false;
	});

	$('#btn-preview').on('click', function () {
		var commentEl = $('#preview-creator-comment');
		var recipientNameEl = $('#preview-recipient-name');
		var amountEl = $('#preview-wish-amount');
		var imageEl = $('#preview-wish-image');
		var linkEl = $('#preview-wish-link');
		var titleEl = $('#preview-wish-title');

		commentEl.text($('#link4').val());
		recipientNameEl.text(WISH_RECIPIENT_NAME);
		amountEl.text(WISH_AMOUNT);

		if (WISH_IMAGES.length > 0) imageEl.css('background-image', 'url(' + WISH_IMAGES[0] + ')');
		else imageEl.css('background-image', 'none');

		if (WISH_LINK !== "") {
			linkEl.attr('href', WISH_LINK);
			linkEl.text(WISH_LINK);
			linkEl.show();
		} else {
			linkEl.hide();
		}

		titleEl.text(WISH_TITLE);

		openPopupPreview();
		return false;
	});

	// one wish
	$('.one-wish-item').on('click', function () {
		$(this).addClass('is-active').siblings().removeClass('is-active');
	});

	// step2
	$('.js-popup-mail').on('click', function () {
		$(".popup-mail").slideDown(); // открыли
		$(".popup-sign-up").slideUp(); // закрыли
		$(".popup-forgot-step1").slideUp(); // закрыли
		$(".popup-forgot-step2").slideUp(); // закрыли
	});
	$('.js-sign-in').on('click', function () {
		$(".sign-in").slideDown(); // открыли
		$(".popup-sign-up").slideUp(); // закрыли
		$(".popup-mail").slideUp(); // закрыли
		$(".popup-forgot-step1").slideUp(); // закрыли
		$(".popup-forgot-step2").slideUp(); // закрыли
	});
	$('.js-popup-sign').on('click', function () {
		$(".popup-sign-up").slideDown(); // открыли
		$(".popup-mail").slideUp(); // закрыли
		$(".sign-in").slideUp(); // закрыли
		$(".popup-forgot-step1").slideUp(); // закрыли
		$(".popup-forgot-step2").slideUp(); // закрыли
	});
	$('.js-forgot-pass').on('click', function () {
		$(".popup-forgot-step1").slideDown(); // открыли
		$(".popup-sign-up").slideUp(); // закрыли
		$(".popup-mail").slideUp(); // закрыли
		$(".sign-in").slideUp(); // закрыли
		$(".popup-forgot-step2").slideUp(); // закрыли
	});


	function updateText(event) {
		var input = $(this);
		setTimeout(function () {
			var val = input.val();
			if (val != "")
				input.parent().addClass("floating-placeholder-float");
			else if (!$(".floating-placeholder").hasClass("m-link"))
				input.parent().removeClass("floating-placeholder-float");
			// else
			// 	input.parent().addClass("floating-placeholder-float");
		}, 100)
	}
	$(".floating-placeholder input, .floating-placeholder textarea").keydown(updateText);
	$(".floating-placeholder input, .floating-placeholder textarea").change(updateText);

	// tooltip error
	$(".js-tooltip-error").hover(
		function () {
			$(".tooltip-error").stop();
			$(".tooltip-error").fadeIn(400);
		}, function () {
			$(".tooltip-error").stop();
			$(".tooltip-error").fadeOut(400);
		}
	);

	// tooltip hint
	$( ".js-tooltip-hint" ).hover(
		function() {
			$(".tooltip-hint").stop();
			$(".tooltip-hint").fadeIn(400);
		}, function() {
			$(".tooltip-hint").stop();
			$(".tooltip-hint").fadeOut(400);
		}
	);

	// tooltip hint2
	$(".js-tooltip-hint2").hover(
		function () {
			$(".tooltip-hint2").stop();
			$(".tooltip-hint2").fadeIn(400);
		}, function () {
			$(".tooltip-hint2").stop();
			$(".tooltip-hint2").fadeOut(400);
		}
	);

	// footer lang
	$('.footer-lang').on('click', function () {
		$(this).toggleClass("is-click");
		$(".lang-tooltip").slideToggle("fast");
	});
	$(document).on('click', function (e) {
		if ($(e.target).closest(".footer-lang").length === 0) {
			$(".footer-lang").removeClass("is-click");
			$(".lang-tooltip").slideUp("fast");
		}
	});

	// tooltip hover
	$(".js-tooltip").hover(
		function () {
			$(".tooltip-open").stop();
			$(".tooltip-open").fadeIn(400);
			$(this).addClass("is-hover");
		}, function () {
			$(".tooltip-open").stop();
			$(".tooltip-open").fadeOut(400);
			$(this).removeClass("is-hover");
		}
	);


	// validate form
	$('.js-sendform').on('click', function () {

		$('.js-validateform .input input').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $(".js-validateform");

			// validation input
			if (val != "") {
				input.addClass("is-succes");
				input.removeClass("is-error");
			} else {
				input.addClass("is-error");
				input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
				input.parent().addClass("wobble animated");

				var delay = setTimeout(function () {
					input.parent().removeClass("wobble animated");
				}, 800)
			} else {
				input.parent().removeClass("wobble animated");
			}
		});

		var input = $(".js-validateform .input input");
		var form = $(".js-validateform");

		// validation form
		if (!$(input).hasClass("is-error")) {
			form.addClass("is-succes");
			form.removeClass("is-error");
		} else {
			form.addClass("is-error");
			form.removeClass("is-succes");
		}
		return false;
	});

	// validate form
	$('.js-sendform2').on('click', function () {

		$('.js-validateform2 .input input').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $(".js-validateform2");

			// validation input
			if (val != "") {
				input.addClass("is-succes");
				input.removeClass("is-error");
			} else {
				input.addClass("is-error");
				input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
				input.parent().addClass("wobble animated");

				var delay = setTimeout(function () {
					input.parent().removeClass("wobble animated");
				}, 800)
			} else {
				input.parent().removeClass("wobble animated");
			}
		});

		var input = $(".js-validateform2 .input input");
		var form = $(".js-validateform2");

		// validation form
		if (!$(input).hasClass("is-error")) {
			form.addClass("is-succes");
			form.removeClass("is-error");
		} else {
			form.addClass("is-error");
			form.removeClass("is-succes");
		}
		return false;
	});

	// validate form
	$('.js-reestablish').on('click', function () {

		$('.popup-forgot-step1 .input input').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $(".popup-forgot-step1");

			// validation input
			if (val != "") {
				input.addClass("is-succes");
				input.removeClass("is-error");
			} else {
				input.addClass("is-error");
				input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
				input.parent().addClass("wobble animated");

				var delay = setTimeout(function () {
					input.parent().removeClass("wobble animated");
				}, 800)
			} else {
				input.parent().removeClass("wobble animated");
			}
		});

		var input = $(".popup-forgot-step1 .input input");
		var form = $(".popup-forgot-step1");

		// validation form
		if (!$(input).hasClass("is-error")) {
			form.addClass("is-succes");
			form.removeClass("is-error");
		} else {
			form.addClass("is-error");
			form.removeClass("is-succes");
		}

		// next step
		if ($(".popup-forgot-step1").hasClass("is-succes")) {
			$(".popup-forgot-step2").slideDown(); // открыли
			$(".popup-sign-up").slideUp(); // закрыли
			$(".popup-mail").slideUp(); // закрыли
			$(".sign-in").slideUp(); // закрыли
			$(".popup-forgot-step1").slideUp(); // закрыли
		}
		return false;
	});

	$('.one-wish-item').on('click', function () {
		if ($(this).hasClass("is-special")) {
			if ($(this).hasClass("is-active")) {
				$(".one-wish-hide-meta").show();
				$(".one-wish-hide .btn-success").css("display", "inline-block");
			}
		}

		if ($(this).hasClass("is-standart")) {
			if ($(this).hasClass("is-active")) {
				$(".one-wish-hide-meta").hide();
			}
		}
	});

	// TODO: Don't accept unknown Content-Types
	//window.Dropzone;
	Dropzone.autoDiscover = true;
	Dropzone.options.fbDropZone = {
		init: function () {
			fbDropZone = this;
			this.on('sending', function (file, xhr, formData) {
				formData.append('wishId', WISH_ID);
			});
			var i;
			for (i = 0; i < WISH_IMAGES.length; i++) {
				var imgUrl = WISH_IMAGES[i];
				// TODO: Set correct size
				var mockFile = { name: imgUrl.substring(imgUrl.lastIndexOf('/') + 1), size: 12345 };
				this.emit('addedfile', mockFile);
				this.emit('thumbnail', mockFile, imgUrl);
				this.emit('complete', mockFile);
				var mF = this.options.maxFiles;
				// TODO: Check
				if (mF - 1 > 0) this.options.maxFiles = mF - 1;
			}
		},
		previewTemplate: '<div class="dz-preview dz-file-preview"><img data-dz-thumbnail /></div>',
		previewsContainer: ".file-dropzone-list",
		paramName: "file",
		addRemoveLinks: "dictRemoveFile",
		thumbnailWidth: null,
		thumbnailHeight: null,
		maxFilesize: 10,
		maxFiles: 6,
		autoProcessQueue: true,
		uploadMultiple: false,
		acceptedFiles: '.png,.jpg,.gif,.jpeg'
	};

	// (function () {
	//     [].slice.call(document.querySelectorAll('select.cs-select')).forEach(function (el) {
	//         new SelectFx(el);
	//     });
	// })();

});
