$(document).ready(function () {

	// open user nav
	$('.js-user-login').on('click', function() {
		$(this).parents("header").find(".user-login").toggleClass("is-open");
		$(this).parents("header").find(".user-subnav").toggleClass("is-open");
		$(".user-overlay").toggleClass("is-active");
		$(".user-subnav").slideToggle(200);
		return false;
	});
	// close user nav
	$(document).on('click', function (e) {
		if ($(e.target).closest(".user-login").length === 0) {
			$(".user-login").removeClass("is-open");
			$(".user-subnav").removeClass("is-open");
			$(".user-overlay").removeClass("is-active");
			$(".user-subnav").slideUp(200);
		}
	});

	// open mobile nav
	$('.js-nav-mobile').on('click', function() {
		$(this).parents("header").find(".nav").slideToggle();
		$("body").toggleClass("is-overlay");
		$(this).toggleClass("is-active");
	});
	// mobile nav, close
	$('.nav-overlay').on('click', function () {
		$(".nav").slideUp();
		$("body").removeClass("is-overlay");
		$(".js-nav-mobile").removeClass("is-active");
	});

});