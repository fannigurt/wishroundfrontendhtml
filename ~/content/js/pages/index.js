function openPopupCountry() {
	$("#country").addClass("md-show");

	$("body").addClass("is-popup");
	$("html").addClass("is-popup");
}
function initPopupHeight() {
	// var bodyHeight = $("body").height();
	var bodyHeight = screen.height;
	$(".md-modal .md-overlay").css("min-height", bodyHeight);
}
function initClosePopup() {
	$('.md-close').on('click', function() {
		$("body").removeClass("is-popup");
		$("html").removeClass("is-popup");
		$(".md-modal").removeClass("md-show");
		// $(".nav-mobile").removeClass("is-active");
	});
	$('.md-overlay').on('click', function() {
		$(".md-modal").removeClass("md-show");
		$("body").removeClass("is-popup");
		$("html").removeClass("is-popup");
		// $(".nav-mobile").removeClass("is-active");
	});
}

function openPopupSignUp() {
	$("#sign-up").addClass("md-show");

	$("body").addClass("is-popup");
	$("html").addClass("is-popup");
}
function initPopupSignUp() {
	$('.header-logout').on('click', function () {
		openPopupSignUp();
		return false;
	});
}

function footerMarginCookie() {
	var cookieHeight = $(".cookie").height();
	var footer = $("footer");

	if (!$(".cookie").hasClass("is-closed")) {
		footer.css("padding-bottom", cookieHeight);
		// footer.css('transform',"translate3d(0px, " + cookieHeight + "px, 0px)");
	}
}

function footerMarginClear() {
	$("footer").css("padding-bottom", "0");
}



$(document).ready(function () {

	initPopupHeight();
	initClosePopup();

	// sign-in BEGIN
	initPopupSignUp();

	footerMarginCookie();

	$('.js-popup-country').on('click', function () {
		openPopupCountry();
	});

	$('.js-popup-mail').on('click', function () {
		$(".popup-mail").slideDown(); // открыли
		$(".popup-sign-up").slideUp(); // закрыли
		$(".popup-forgot-step1").slideUp(); // закрыли
		$(".popup-forgot-step2").slideUp(); // закрыли
	});
	$('.js-sign-in').on('click', function () {
		$(".sign-in").slideDown(); // открыли
		$(".popup-sign-up").slideUp(); // закрыли
		$(".popup-mail").slideUp(); // закрыли
		$(".popup-forgot-step1").slideUp(); // закрыли
		$(".popup-forgot-step2").slideUp(); // закрыли
	});
	$('.js-popup-sign').on('click', function () {
		$(".popup-sign-up").slideDown(); // открыли
		$(".popup-mail").slideUp(); // закрыли
		$(".sign-in").slideUp(); // закрыли
		$(".popup-forgot-step1").slideUp(); // закрыли
		$(".popup-forgot-step2").slideUp(); // закрыли
	});
	$('.js-forgot-pass').on('click', function () {
		$(".popup-forgot-step1").slideDown(); // открыли
		$(".popup-sign-up").slideUp(); // закрыли
		$(".popup-mail").slideUp(); // закрыли
		$(".sign-in").slideUp(); // закрыли
		$(".popup-forgot-step2").slideUp(); // закрыли
	});
	// validate form
	$('.js-sendform').on('click', function () {

		$('.js-validateform .input input').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $(".js-validateform");

			// validation input
			if (val != "") {
				input.addClass("is-succes");
				input.removeClass("is-error");
			} else {
				input.addClass("is-error");
				input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
				input.parent().addClass("wobble animated");

				var delay = setTimeout(function () {
					input.parent().removeClass("wobble animated");
				}, 800)
			} else {
				input.parent().removeClass("wobble animated");
			}
		});

		var input = $(".js-validateform .input input");
		var form = $(".js-validateform");

		// validation form
		if (!$(input).hasClass("is-error")) {
			form.addClass("is-succes");
			form.removeClass("is-error");
		} else {
			form.addClass("is-error");
			form.removeClass("is-succes");
		}
		return false;
	});
	// validate form
	$('.js-reestablish').on('click', function () {

		$('.popup-forgot-step1 .input input').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $(".popup-forgot-step1");

			// validation input
			if (val != "") {
				input.addClass("is-succes");
				input.removeClass("is-error");
			} else {
				input.addClass("is-error");
				input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
				input.parent().addClass("wobble animated");

				var delay = setTimeout(function () {
					input.parent().removeClass("wobble animated");
				}, 800)
			} else {
				input.parent().removeClass("wobble animated");
			}
		});

		var input = $(".popup-forgot-step1 .input input");
		var form = $(".popup-forgot-step1");

		// validation form
		if (!$(input).hasClass("is-error")) {
			form.addClass("is-succes");
			form.removeClass("is-error");
		} else {
			form.addClass("is-error");
			form.removeClass("is-succes");
		}

		// next step
		if ($(".popup-forgot-step1").hasClass("is-succes")) {
			$(".popup-forgot-step2").slideDown(); // открыли
			$(".popup-sign-up").slideUp(); // закрыли
			$(".popup-mail").slideUp(); // закрыли
			$(".sign-in").slideUp(); // закрыли
			$(".popup-forgot-step1").slideUp(); // закрыли
		}
		return false;
	});

	// validate form
	$('.js-sendform2').on('click', function () {

		$('.js-validateform2 .input input').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $(".js-validateform2");

			// validation input
			if (val != "") {
				input.addClass("is-succes");
				input.removeClass("is-error");
			} else {
				input.addClass("is-error");
				input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
				input.parent().addClass("wobble animated");

				var delay = setTimeout(function () {
					input.parent().removeClass("wobble animated");
				}, 800)
			} else {
				input.parent().removeClass("wobble animated");
			}
		});

		var input = $(".js-validateform2 .input input");
		var form = $(".js-validateform2");

		// validation form
		if (!$(input).hasClass("is-error")) {
			form.addClass("is-succes");
			form.removeClass("is-error");
		} else {
			form.addClass("is-error");
			form.removeClass("is-succes");
		}
		// next step
		if ($(".popup-forgot-step1").hasClass("is-succes")) {
			$(".popup-forgot-step2").slideDown(); // открыли
			$(".popup-sign-up").slideUp(); // закрыли
			$(".popup-mail").slideUp(); // закрыли
			$(".sign-in").slideUp(); // закрыли
			$(".popup-forgot-step1").slideUp(); // закрыли
		}
		return false;
	});
	// tooltip hint2
	$(".js-tooltip-sign-birthday").hover(
		function () {
			$(".tooltip-sign-birthday").stop();
			$(".tooltip-sign-birthday").fadeIn(400);
		}, function () {
			$(".tooltip-sign-birthday").stop();
			$(".tooltip-sign-birthday").fadeOut(400);
		}
	);
	// sign-in END

	function updateText(event) {
		var input = $(this);
		setTimeout(function () {
			var val = input.val();
			if (val != "")
				input.parent().addClass("floating-placeholder-float");
			else if (!$(".floating-placeholder").hasClass("m-link"))
				input.parent().removeClass("floating-placeholder-float");
			// else
			// 	input.parent().addClass("floating-placeholder-float");
		}, 100)
	}
	$(".floating-placeholder input, .floating-placeholder textarea").keydown(updateText);
	$(".floating-placeholder input, .floating-placeholder textarea").change(updateText);

	function scrollIndex() {
		if ($(window).width() > 980) {
			smoothScroll.init({
				offset: 60
			});
		} else {
			smoothScroll.init({
				offset: 0
			});
		}
	}
	scrollIndex();

	// function indexSlide() {
	// 	var windowHeight = $(window).height();
	// 	var headerHeight = $("header").outerHeight();
	// 	// var slideHeight = windowHeight - headerHeight;
	// 	var slideHeight = windowHeight;

	// 	if ($(window).height() > 380) {
	// 		// set height for slide on index page
	// 		$(".slider .slider-item").css("height", slideHeight);
	// 		$(".slider .table").css("height", slideHeight);
	// 	}
	// }
	// if ($(".slider").length > 0) {
	// 	indexSlide();
	// }

	// big slider
	function slick() {
		$('.js-slick').slick({
			dots: true,
			arrows: false,
			infinite: true,
			slidesToShow: 1,
			// autoplay: true,
			// autoplaySpeed: 10000
			autoplay: false
		});
	}
	if ($(".js-slick").length > 0) {
		slick();
	}
	$('.js-slider-prev').click(function(){
	  $(".js-slick").slickPrev();
	});
	$('.js-slider-next').click(function(){
	  $(".js-slick").slickNext();
	});



	// slider shops
	function slick2() {
		$('.js-shop').slick({
			dots: false,
			infinite: true,
			slidesToShow: 4,
			responsive: [
			{
				breakpoint: 1150,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 800,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 580,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
			]
		});
	}
	if ($(".js-shop").length > 0) {
		slick2();
	}

	// open mobile nav
	$('.js-nav-mobile').on('click', function() {
		$(this).parents("header").find(".nav").slideToggle();
		$("body").toggleClass("is-overlay");
		$(this).toggleClass("is-active");
	});

	// mobile nav, close
	$('.nav-overlay').on('click', function () {
		$(".nav").slideUp();
		$("body").removeClass("is-overlay");
		$(".js-nav-mobile").removeClass("is-active");
	});

	// index mobile close nav
	function indexMobileCloseNav() {
		if ($(window).width() < 960) {
			$('.index .nav-list a').on('click', function () {
				$(".nav").slideUp();
				$("body").removeClass("is-overlay");
				$(".js-nav-mobile").removeClass("is-active");
			});
		};
	}
	indexMobileCloseNav();

	function animateIndex() {

		if ($(window).width() > 980) {
			// #about-us animate
			var height1 = $("#about-us").height() / 2 + 50;
			var height2 = $("#create-wish").outerHeight();

			var position = height1 + height2;

			if ($(window).scrollTop() > position) {
				$("#how-work").addClass('is-animate');

			} else {
				$("#how-work").removeClass('is-animate');
			}

			// #partners animate
			position2 = $(".quote").offset().top + 300;

			if ($(window).scrollTop() > position2) {
				$("#partners").addClass('is-animate');

			} else {
				$("#partners").removeClass('is-animate');
			}

			// create-wish animate
			var position3 = $("#create-wish").offset().top - 120;

			if ($(window).scrollTop() > position3) {


				if (!$(".create-wish-items").hasClass("is-animated")) {
					$(".create-wish-items").addClass("is-animated");
					var $item = $(".create-wish-item").addClass('animated pulse');

					setTimeout(function() {
					     $item.removeClass('animated pulse');
					 }, 1600);
				}
			}

		}
	}
	if ($(".quote").length > 0) {
		animateIndex();
	}

	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		var expires = "expires=" + d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	}

	$(function () {
		$('.cookie-succes').on('click', function () {
			setCookie("WishroundCA", "1", 365);
			$(".cookie").slideUp();

			$(".cookie").addClass("is-closed");
			footerMarginClear();

			return false;
		});
	});

	// tooltip error
	$( ".js-tooltip-error" ).hover(
		function() {
			$(".tooltip-error").stop();
			$(".tooltip-error").fadeIn(400);
		}, function() {
			$(".tooltip-error").stop();
		 $(".tooltip-error").fadeOut(400);
		}
	);

	// footer lang
	$('.footer-lang').on('click', function() {
		$(this).toggleClass("is-click");
		$(".lang-tooltip").slideToggle("fast");
	});
	$(document).on('click', function (e) {
		if ($(e.target).closest(".footer-lang").length === 0) {
			$(".footer-lang").removeClass("is-click");
			$(".lang-tooltip").slideUp("fast");
		}
	});

	// tooltip hover
	$( ".js-tooltip" ).hover(
		function() {
			$(".tooltip-open").stop();
			$(".tooltip-open").fadeIn(400);
			$(this).addClass("is-hover");
		}, function() {
			$(".tooltip-open").stop();
			$(".tooltip-open").fadeOut(400);
			$(this).removeClass("is-hover");
		}
	);

	$(window).resize(function () {
		// if ($(".slider").length > 0) {
		// 	indexSlide();
		// }
		indexMobileCloseNav();
		footerMarginCookie();
	});

	$(window).scroll(function () {
		if ($(".quote").length > 0) {
			animateIndex();
		}
		scrollIndex();
	});

	// open user nav
	$('.js-user-login').on('click', function() {
		$(this).parents("header").find(".user-login").toggleClass("is-open");
		$(this).parents("header").find(".user-subnav").toggleClass("is-open");
		$(".user-overlay").toggleClass("is-active");
		$(".user-subnav").slideToggle(200);
		return false;
	});
	// close user nav
	$(document).on('click', function (e) {
		if ($(e.target).closest(".user-login").length === 0) {
			$(".user-login").removeClass("is-open");
			$(".user-subnav").removeClass("is-open");
			$(".user-overlay").removeClass("is-active");
			$(".user-subnav").slideUp(200);
		}
	});

});