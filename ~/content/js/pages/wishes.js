function tab() {
	$(".js-tab").each(function(){
		var tab_link = $(this).find("a");
		var tab_item = $(this).find("li");
		var tab_cont = $(this).parents(".js-tab-group").find(".js-tab-cont");
		// tab_cont.hide(); 
		tab_item.first().addClass("is-active");
		$(this).parents(".js-tab-group").find(".js-tab1").addClass("is-active");
		tab_link.on("click", function() {
			var index = $(this).attr("href");
			tab_item.removeClass("is-active");
			$(this).parent().addClass("is-active");
			tab_cont.removeClass("is-active");
			$(this).parents(".js-tab-group").find("."+index).addClass("is-active");
			return false;
		});
	});
}
// 1
function tab1Content1Open() {
	$(".js-tab1-content1").slideDown(200);
	$(".js-tab1-content2").slideUp(200);

	$("#switcher1-link1").addClass("is-active");
	$("#switcher1-link2").removeClass("is-active");
}
function tab1Content2Open() {
	$(".js-tab1-content2").slideDown(200);
	$(".js-tab1-content1").slideUp(200);

	$("#switcher1-link1").removeClass("is-active");
	$("#switcher1-link2").addClass("is-active");
}
// 2
function tab2Content1Open() {
	$(".js-tab2-content1").slideDown(200);
	$(".js-tab2-content2").slideUp(200);

	$("#switcher2-link1").addClass("is-active");
	$("#switcher2-link2").removeClass("is-active");
}
function tab2Content2Open() {
	$(".js-tab2-content2").slideDown(200);
	$(".js-tab2-content1").slideUp(200);

	$("#switcher2-link1").removeClass("is-active");
	$("#switcher2-link2").addClass("is-active");
}
// 3
function tab3Content1Open() {
	$(".js-tab3-content1").slideDown(200);
	$(".js-tab3-content2").slideUp(200);

	$("#switcher3-link1").addClass("is-active");
	$("#switcher3-link2").removeClass("is-active");
}
function tab3Content2Open() {
	$(".js-tab3-content2").slideDown(200);
	$(".js-tab3-content1").slideUp(200);

	$("#switcher3-link1").removeClass("is-active");
	$("#switcher3-link2").addClass("is-active");
}
// 4
function tab4Content1Open() {
	$(".js-tab4-content1").slideDown(200);
	$(".js-tab4-content2").slideUp(200);

	$("#switcher4-link1").addClass("is-active");
	$("#switcher4-link2").removeClass("is-active");
}
function tab4Content2Open() {
	$(".js-tab4-content2").slideDown(200);
	$(".js-tab4-content1").slideUp(200);

	$("#switcher4-link1").removeClass("is-active");
	$("#switcher4-link2").addClass("is-active");
}

function showInviteBanner() {
	$(".invite").show();
	$(".getapp").hide();
	$(".profile").hide();
}
function showGetappBanner() {
	$(".getapp").show();
	$(".invite").hide();
	$(".profile").hide();
}
function showprofileBanner() {
	$(".profile").show();
	$(".getapp").hide();
	$(".invite").hide();
}

$(document).ready(function() {
	tab();
	$(".js-tab1-content2").hide();
	$(".js-tab2-content2").hide();
	$(".js-tab3-content2").hide();
	$(".js-tab4-content2").hide();
	// 1
	$('#switcher1-link1').on('click', function () {
		tab1Content1Open();
	});
	$('#switcher1-link2').on('click', function () {
		tab1Content2Open();
	});
	// 2
	$('#switcher2-link1').on('click', function () {
		tab2Content1Open();
	});
	$('#switcher2-link2').on('click', function () {
		tab2Content2Open();
	});
	// 3
	$('#switcher3-link1').on('click', function () {
		tab3Content1Open();
	});
	$('#switcher3-link2').on('click', function () {
		tab3Content2Open();
	});
	// 4
	$('#switcher4-link1').on('click', function () {
		tab4Content1Open();
	});
	$('#switcher4-link2').on('click', function () {
		tab4Content2Open();
	});

	// change banner
	$(".getapp").hide();
	$(".profile").hide();

	$('#tab-nav-2').on('click', function () {
		showGetappBanner();
	});
	$('#tab-nav-1').on('click', function () {
		showInviteBanner();
	});
	$('#tab-nav-3').on('click', function () {
		showInviteBanner(); // upd
	});
	$('#tab-nav-4').on('click', function () {
		showprofileBanner();
	});

	// chart
	$(function() {
		$('.chart').easyPieChart({
			lineWidth: 12,
			size: 134,
			scaleColor: "transparent",
			barColor: "#00BF5F",
			trackColor: "#D4D4D4",
			lineCap: "butt",
			onStep: function(from, to, percent) {
				$(this.el).find('.chart-percent-numm').text(Math.round(percent));
			}
		});
		var chart = window.chart = $('.chart').data('easyPieChart');
		$('.js_update').on('click', function() {
			chart.update(Math.random()*200-100);
		});
	});


	function updateText(event) {
		var input = $(this);
		setTimeout(function () {
			var val = input.val();
			if (val != "")
				input.parent().addClass("floating-placeholder-float");
			else if (!$(".floating-placeholder").hasClass("m-link"))
				input.parent().removeClass("floating-placeholder-float");
			// else
			// 	input.parent().addClass("floating-placeholder-float");
		}, 100)
	}
	$(".floating-placeholder input, .floating-placeholder textarea").keydown(updateText);
	$(".floating-placeholder input, .floating-placeholder textarea").change(updateText);

});