function openPopupHowWork() {
	$("#how-work").addClass("md-show");

	$("body").addClass("is-popup");
	$("html").addClass("is-popup");
}
function openPopupPartners() {
	$("#partners").addClass("md-show");

	$("body").addClass("is-popup");
	$("html").addClass("is-popup");
}
function openPopupCountry() {
	$("#country").addClass("md-show");

	$("body").addClass("is-popup");
	$("html").addClass("is-popup");
}
function initPopupHeight() {
	// var bodyHeight = $("body").height();
	var bodyHeight = screen.height;
	$(".md-modal .md-overlay").css("min-height", bodyHeight);
}
function initClosePopup() {
	$('.md-close').on('click', function() {
		$("body").removeClass("is-popup");
		$("html").removeClass("is-popup");
		$(".md-modal").removeClass("md-show");
		// $(".nav-mobile").removeClass("is-active");
	});
	$('.md-overlay').on('click', function() {
		$(".md-modal").removeClass("md-show");
		$("body").removeClass("is-popup");
		$("html").removeClass("is-popup");
		// $(".nav-mobile").removeClass("is-active");
	});
}
function initPopupCountry() {
	openPopupCountry(); 
}
function initPopupHowWork() {
	var link = $("a[data-modal='how-work']"); 

	link.on('click', function() {
		if ($(this).hasClass("md-trigger")) {
			openPopupHowWork();
			return false;
		}
	});
}
function initPopupPartners() {
	var link = $("a[data-modal='partners']");

	link.on('click', function() {
		if ($(this).hasClass("md-trigger")) {
			openPopupPartners();
			return false;
		}
	});
}

function openPopupSignUp() {
	$("#sign-up").addClass("md-show");

	$("body").addClass("is-popup");
	$("html").addClass("is-popup");
}
function initPopupSignUp() {
	$('.header-logout').on('click', function () {
		openPopupSignUp();
		return false;
	});
}

function footerMarginCookie() {
	var cookieHeight = $(".cookie").height();
	var footer = $("footer");

	if (!$(".cookie").hasClass("is-closed")) {
		footer.css("padding-bottom", cookieHeight);
		// footer.css('transform',"translate3d(0px, " + cookieHeight + "px, 0px)");
	}
}

function footerMarginClear() {
	$("footer").css("padding-bottom", "0");
}

$(document).ready(function () {

	initPopupHeight();
	initClosePopup();
	// initPopupMobile();

	// initPopupCountry();
	initPopupHowWork();
	initPopupPartners();

	footerMarginCookie();

	$(window).resize(function () {
		footerMarginCookie();
	});

	// sign-in BEGIN
	initPopupSignUp();
	$('.js-popup-mail').on('click', function () {
		$(".popup-mail").slideDown(); // открыли
		$(".popup-sign-up").slideUp(); // закрыли
		$(".popup-forgot-step1").slideUp(); // закрыли
		$(".popup-forgot-step2").slideUp(); // закрыли
	});
	$('.js-sign-in').on('click', function () {
		$(".sign-in").slideDown(); // открыли
		$(".popup-sign-up").slideUp(); // закрыли
		$(".popup-mail").slideUp(); // закрыли
		$(".popup-forgot-step1").slideUp(); // закрыли
		$(".popup-forgot-step2").slideUp(); // закрыли
	});
	$('.js-popup-sign').on('click', function () {
		$(".popup-sign-up").slideDown(); // открыли
		$(".popup-mail").slideUp(); // закрыли
		$(".sign-in").slideUp(); // закрыли
		$(".popup-forgot-step1").slideUp(); // закрыли
		$(".popup-forgot-step2").slideUp(); // закрыли
	});
	$('.js-forgot-pass').on('click', function () {
		$(".popup-forgot-step1").slideDown(); // открыли
		$(".popup-sign-up").slideUp(); // закрыли
		$(".popup-mail").slideUp(); // закрыли
		$(".sign-in").slideUp(); // закрыли
		$(".popup-forgot-step2").slideUp(); // закрыли
	});
	// validate form
	$('.js-sendform').on('click', function () {

		$('.js-validateform .input input').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $(".js-validateform");

			// validation input
			if (val != "") {
				input.addClass("is-succes");
				input.removeClass("is-error");
			} else {
				input.addClass("is-error");
				input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
				input.parent().addClass("wobble animated");

				var delay = setTimeout(function () {
					input.parent().removeClass("wobble animated");
				}, 800)
			} else {
				input.parent().removeClass("wobble animated");
			}
		});

		var input = $(".js-validateform .input input");
		var form = $(".js-validateform");

		// validation form
		if (!$(input).hasClass("is-error")) {
			form.addClass("is-succes");
			form.removeClass("is-error");
		} else {
			form.addClass("is-error");
			form.removeClass("is-succes");
		}
		return false;
	});
	// validate form
	$('.js-reestablish').on('click', function () {

		$('.popup-forgot-step1 .input input').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $(".popup-forgot-step1");

			// validation input
			if (val != "") {
				input.addClass("is-succes");
				input.removeClass("is-error");
			} else {
				input.addClass("is-error");
				input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
				input.parent().addClass("wobble animated");

				var delay = setTimeout(function () {
					input.parent().removeClass("wobble animated");
				}, 800)
			} else {
				input.parent().removeClass("wobble animated");
			}
		});

		var input = $(".popup-forgot-step1 .input input");
		var form = $(".popup-forgot-step1");

		// validation form
		if (!$(input).hasClass("is-error")) {
			form.addClass("is-succes");
			form.removeClass("is-error");
		} else {
			form.addClass("is-error");
			form.removeClass("is-succes");
		}

		// next step
		if ($(".popup-forgot-step1").hasClass("is-succes")) {
			$(".popup-forgot-step2").slideDown(); // открыли
			$(".popup-sign-up").slideUp(); // закрыли
			$(".popup-mail").slideUp(); // закрыли
			$(".sign-in").slideUp(); // закрыли
			$(".popup-forgot-step1").slideUp(); // закрыли
		}
		return false;
	});

	// validate form
	$('.js-sendform2').on('click', function () {

		$('.js-validateform2 .input input').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $(".js-validateform2");

			// validation input
			if (val != "") {
				input.addClass("is-succes");
				input.removeClass("is-error");
			} else {
				input.addClass("is-error");
				input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
				input.parent().addClass("wobble animated");

				var delay = setTimeout(function () {
					input.parent().removeClass("wobble animated");
				}, 800)
			} else {
				input.parent().removeClass("wobble animated");
			}
		});

		var input = $(".js-validateform2 .input input");
		var form = $(".js-validateform2");

		// validation form
		if (!$(input).hasClass("is-error")) {
			form.addClass("is-succes");
			form.removeClass("is-error");
		} else {
			form.addClass("is-error");
			form.removeClass("is-succes");
		}
		// next step
		if ($(".popup-forgot-step1").hasClass("is-succes")) {
			$(".popup-forgot-step2").slideDown(); // открыли
			$(".popup-sign-up").slideUp(); // закрыли
			$(".popup-mail").slideUp(); // закрыли
			$(".sign-in").slideUp(); // закрыли
			$(".popup-forgot-step1").slideUp(); // закрыли
		}
		return false;
	});
	// tooltip hint2
	$(".js-tooltip-sign-birthday").hover(
		function () {
			$(".tooltip-sign-birthday").stop();
			$(".tooltip-sign-birthday").fadeIn(400);
		}, function () {
			$(".tooltip-sign-birthday").stop();
			$(".tooltip-sign-birthday").fadeOut(400);
		}
	);
	// sign-in END

	// validate form 
	$('.js-partners-btn').on('click', function () {
    $('.js-partners-form .input input').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $(".js-partners-form");

			// validation input
			if (val != "") {
			    input.addClass("is-succes");
			    input.removeClass("is-error");
			} else {
					input.addClass("is-error");
					input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
			    input.parent().addClass("wobble animated");

			    var delay = setTimeout(function () {
			        input.parent().removeClass("wobble animated");
			    }, 800)
			} else {
				input.parent().removeClass("wobble animated");
			}
		});

    var input = $(".js-partners-form .input input");
    var form = $(".js-partners-form");
		// validation form
		if (!$(input).hasClass("is-error")) {
			form.addClass("is-succes");
			form.removeClass("is-error");
		} else {
			form.addClass("is-error");
			form.removeClass("is-succes");
		}

		// close popup 
		if ($(".js-partners-form").hasClass("is-succes")) {
			$("#partners").removeClass("md-show");

			$("body").removeClass("is-popup");
			$("html").removeClass("is-popup");
		}
		return false;
	});

	function updateText(event){
		var input=$(this);
		setTimeout(function(){
			var val=input.val();
			if(val!="")
				input.parent().addClass("floating-placeholder-float");
			else if (!$(".floating-placeholder").hasClass("m-link"))
				input.parent().removeClass("floating-placeholder-float");
			// else
			// 	input.parent().addClass("floating-placeholder-float");
		},100)
	}
	$(".floating-placeholder input, .floating-placeholder textarea").keydown(updateText);
	$(".floating-placeholder input, .floating-placeholder textarea").change(updateText);

	// open mobile nav
	$('.js-nav-mobile').on('click', function() {
		$(this).parents("header").find(".nav").slideToggle();
		$("body").toggleClass("is-overlay");
		$(this).toggleClass("is-active");
	});

	// mobile nav, close
	$('.nav-overlay').on('click', function () {
		$(".nav").slideUp();
		$("body").removeClass("is-overlay");
		$(".js-nav-mobile").removeClass("is-active");
	});

	// footer lang
	$('.footer-lang').on('click', function() {
		$(this).toggleClass("is-click");
		$(".lang-tooltip").slideToggle("fast");
	});
	$(document).on('click', function (e) {
		if ($(e.target).closest(".footer-lang").length === 0) {
			$(".footer-lang").removeClass("is-click");
			$(".lang-tooltip").slideUp("fast");
		}
	});

	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		var expires = "expires=" + d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	}

	$(function () {
		$('.cookie-succes').on('click', function () {
			setCookie("WishroundCA", "1", 365);
			$(".cookie").slideUp();

			$(".cookie").addClass("is-closed");
			footerMarginClear();

			return false;
		});
	});

	// faq
	$('.js-faq-list li').on('click', function() {
		if ($(this).hasClass("is-active")) {
			$(this).removeClass("is-active");
		} else {
			$(this).addClass('is-active').siblings().removeClass('is-active');
		}
	});

});