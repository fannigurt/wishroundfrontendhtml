function openPopupConnect() {
	$("#connect").addClass("md-show");

	$("body").addClass("is-popup");
	$("html").addClass("is-popup");
}
function openPopupConnectSuccess() {
	$("#connect-success").addClass("md-show");

	$("body").addClass("is-popup");
	$("html").addClass("is-popup");
}
function initPopupHeight() {
	// var bodyHeight = $("body").height();
	var bodyHeight = screen.height;
	$(".md-modal .md-overlay").css("min-height", bodyHeight);
}
// function initPopupMobile() {
// 	$('.md-trigger').on('click', function () {
// 		if ($(window).width() < 960) {
// 			$("body").removeClass("is-overlay");
// 			$(".nav-list").slideUp();

// 			$('.md-modal .md-content').each(function () {
// 				var heightPopupContent = $(this).outerHeight();
// 				var heightPopup = heightPopupContent + 60;
// 				var minheightOverlay = $(this).parent().find(".md-overlay").css("min-height", heightPopup);
// 			});
// 		}
// 	});
// }
function initClosePopup() {
	$('.md-close').on('click', function() {
		$("body").removeClass("is-popup");
		$("html").removeClass("is-popup");
		$(".md-modal").removeClass("md-show");
		$(".nav-mobile").removeClass("is-active");
	});
	$('.md-overlay').on('click', function() {
		$(".md-modal").removeClass("md-show");
		$("body").removeClass("is-popup");
		$("html").removeClass("is-popup");
		$(".nav-mobile").removeClass("is-active");
	});
}
function initPopupConnect() {
	var link = $("a[data-modal='connect']"); 

	link.on('click', function() {
		if ($(this).hasClass("md-trigger")) {
			openPopupConnect();
			return false;
		}
	});
}
function initPopupConnectSuccess() {
	var link = $("a[data-modal='connect-success']"); 

	link.on('click', function() {
		if ($(this).hasClass("md-trigger")) {
			openPopupConnectSuccess();
			$("#connect").removeClass("md-show");
			// $("body").removeClass("is-popup");
			// $("html").removeClass("is-popup");
			return false;
		}
	});
}

function footerMarginCookie() {
	var cookieHeight = $(".cookie").height();
	var footer = $("footer");

	if (!$(".cookie").hasClass("is-closed")) {
		footer.css("padding-bottom", cookieHeight);
		// footer.css('transform',"translate3d(0px, " + cookieHeight + "px, 0px)");
	}
}

function footerMarginClear() {
	$("footer").css("padding-bottom", "0");
}


$(document).ready(function () {

	smoothScroll.init();

	initPopupHeight();
	initClosePopup();
	// initPopupMobile();

	initPopupConnect();
	initPopupConnectSuccess(); 

	footerMarginCookie();

	$(window).resize(function () {
		footerMarginCookie();
	});

	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		var expires = "expires=" + d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	}

	$(function () {
		$('.cookie-succes').on('click', function () {
			setCookie("WishroundCA", "1", 365);
			$(".cookie").slideUp();

			$(".cookie").addClass("is-closed");
			footerMarginClear();

			return false;
		});
	});

	// validate form 
	$('.js-connect-success').on('click', function () {
    $('.js-connect-form .input input').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $(".js-connect-form");

			// validation input
			if (val != "") {
			    input.addClass("is-succes");
			    input.removeClass("is-error");
			} else {
					input.addClass("is-error");
					input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
			    input.parent().addClass("wobble animated");

			    var delay = setTimeout(function () {
			        input.parent().removeClass("wobble animated");
			    }, 800)
			} else {
				input.parent().removeClass("wobble animated");
			}
		});

    var input = $(".js-connect-form .input input");
    var form = $(".js-connect-form");
		// validation form
		if (!$(input).hasClass("is-error")) {
			form.addClass("is-succes");
			form.removeClass("is-error");
		} else {
			form.addClass("is-error");
			form.removeClass("is-succes");
		}

		// open #connect-success popup
		if ($(".js-connect-form").hasClass("is-succes")) {
			$("#connect").removeClass("md-show");
			$("#connect-success").addClass("md-show");
		}
		return false;
	});

	// animated shake
	// wobble animated

	// validate form 
	$('#js-merchform-connect').on('click', function () {
    $('#merchform input').each(function () {
			var input = $(this);
			var val = input.val();
			var form = $("#merchform");

			// validation input
			if (val != "") {
				input.addClass("is-succes");
				input.removeClass("is-error");
			} else {
					input.addClass("is-error");
					input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
					input.addClass("bounce animated");

					var delay = setTimeout(function () {
						input.removeClass("bounce animated");
					}, 800)
			} else {
				input.removeClass("bounce animated");
			}


		});

		// validation form
		if (!$('#merchform input').hasClass("is-error")) {
			$("#merchform").addClass("is-succes");
			$("#merchform").removeClass("is-error");
		} else {
			$("#merchform").addClass("is-error");
			$("#merchform").removeClass("is-succes");
		} 

		// open #connect-success popup
		if ($("#merchform").hasClass("is-succes")) {
			$("#connect-success").addClass("md-show");
		}
		return false;
	});

	// tab 
	function tab() {
		$(".js-tab").each(function(){
			var tab_link = $(this).find("a");
			var tab_item = $(this).find("li");
			var tab_cont = $(this).parents(".js-tab-group").find(".js-tab-cont");
			// tab_cont.hide(); 
			tab_item.first().addClass("is-active");
			$(this).parents(".js-tab-group").find(".js-tab1").addClass("is-active");
			tab_link.on("click", function() {
				var index = $(this).attr("href");
				tab_item.removeClass("is-active");
				$(this).parent().addClass("is-active");
				tab_cont.removeClass("is-active");
				$(this).parents(".js-tab-group").find("."+index).addClass("is-active");
				return false;
			});
			});
	} tab();

	// footer lang
	$('.footer-lang').on('click', function() {
		$(this).toggleClass("is-click");
		$(".lang-tooltip").slideToggle("fast");
	});
	$(document).on('click', function (e) {
		if ($(e.target).closest(".footer-lang").length === 0) {
			$(".footer-lang").removeClass("is-click");
			$(".lang-tooltip").slideUp("fast");
		}
	});

	$('.js-merch-form').on('click', function() {
		$(".merchcontact").fadeOut(600);
		$(".merchform").fadeIn(600);
		return false;
	});


	function updateText(event){
		var input=$(this);
		setTimeout(function(){
			var val=input.val();
			if(val!="")
				input.parent().addClass("floating-placeholder-float");
			else if (!$(".floating-placeholder").hasClass("m-link"))
				input.parent().removeClass("floating-placeholder-float");
			// else
			// 	input.parent().addClass("floating-placeholder-float");
		},100)
	}
	$(".floating-placeholder input, .floating-placeholder textarea").keydown(updateText);
	$(".floating-placeholder input, .floating-placeholder textarea").change(updateText);



});

