function openPopupCloseWish() {
	$("#close-wish").addClass("md-show");

	$("body").addClass("is-popup");
	$("html").addClass("is-popup");
}

function initPopupHeight() {
	// var bodyHeight = $("body").height();
	var bodyHeight = screen.height;
	$(".md-modal .md-overlay").css("min-height", bodyHeight);
}

function initClosePopup() {
	$('.md-close').on('click', function() {
		$("body").removeClass("is-popup");
		$("html").removeClass("is-popup");
		$(".md-modal").removeClass("md-show");
		$(".nav-mobile").removeClass("is-active");
	});
	$('.md-overlay').on('click', function() {
		$(".md-modal").removeClass("md-show");
		$("body").removeClass("is-popup");
		$("html").removeClass("is-popup");
		$(".nav-mobile").removeClass("is-active");
	});
}

function initPopupCloseWish() {
	var link = $("a[data-modal='close-wish']"); 

	link.on('click', function() {
		if ($(this).hasClass("md-trigger")) {
			openPopupCloseWish();
			return false;
		}
	});
}

// function initPopupAskMeLater() {
// 	var link = $(".js-ask-later");

// 	link.on('click', function() {
// 		$("#close-wish").removeClass("md-show");
// 		$("body").removeClass("is-popup");
// 		$("html").removeClass("is-popup");

// 		$(".js-wishinfo").addClass("is-active");
// 		$(".wishoverlay").addClass("is-active");
// 		$(".wish").addClass("is-overlay");


// 		// return false;
// 	});
// }

function msieversion() {
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf("MSIE ");

  if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
  	  console.log(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
  		$("body").addClass("ie");
  }
  else {
  	// console.log('otherbrowser');
  }
  return false;
}

$(document).ready(function () {
	initPopupHeight();
	initClosePopup();
	// initPopupMobile();

	initPopupCloseWish();
	// initPopupAskMeLater();

	msieversion();

	// footer lang
	$('.footer-lang').on('click', function() {
		$(this).toggleClass("is-click");
		$(".lang-tooltip").slideToggle("fast");
	});
	$(document).on('click', function (e) {
		if ($(e.target).closest(".footer-lang").length === 0) {
			$(".footer-lang").removeClass("is-click");
			$(".lang-tooltip").slideUp("fast");
		}
	});

	// open user nav
	$('.js-user-login').on('click', function() {
		$(this).parents("header").find(".user-login").toggleClass("is-open");
		$(this).parents("header").find(".user-subnav").toggleClass("is-open");
		$(".user-overlay").toggleClass("is-active");
		$(".user-subnav").slideToggle(200);
		return false;
	});
	// close user nav
	$(document).on('click', function (e) {
		if ($(e.target).closest(".user-login").length === 0) {
			$(".user-login").removeClass("is-open");
			$(".user-subnav").removeClass("is-open");
			$(".user-overlay").removeClass("is-active");
			$(".user-subnav").slideUp(200);
		}
	});

		// validate form 
		$('#js-merchform-connect').on('click', function () {
	    $('#wishform input').each(function () {
				var input = $(this);
				var val = input.val();
				var form = $("#wishform");

				// validation input
				if (val != "") {
					input.addClass("is-succes");
					input.removeClass("is-error");
				} else {
						input.addClass("is-error");
						input.removeClass("is-succes");
				}

				// input animation
				if (input.hasClass("is-error")) {
						input.addClass("bounce animated");

						var delay = setTimeout(function () {
							input.removeClass("bounce animated");
						}, 800)
				} else {
					input.removeClass("bounce animated");
				}


			});

			// validation form
			if (!$('#wishform input').hasClass("is-error")) {
				$("#wishform").addClass("is-succes");
				$("#wishform").removeClass("is-error");
			} else {
				$("#wishform").addClass("is-error");
				$("#wishform").removeClass("is-succes");
			} 

			// open #connect-success popup
			if ($("#wishform").hasClass("is-succes")) {
				$(".wishinfo-form").hide();
				$(".wishinfo-thanks").show();
			}
			return false;
		});

});