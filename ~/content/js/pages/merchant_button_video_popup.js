function initPopupHeight() {
  // var bodyHeight = $("body").height();
  var bodyHeight = screen.height;
	$(".md-modal .md-overlay").css("min-height", bodyHeight);
}

function initClosePopup() {
  $('.wishroundMerchantCloseBtn').on('click', function () {
    parent.window.postMessage("removetheiframe", "*");
  });
}

function initClosePopupButton() {
  $('.md-close').on('click', function () {
    $("body").removeClass("is-popup");
    $("html").removeClass("is-popup");
    $(".md-modal").removeClass("md-show");

    resetModifyMerchantPopup();
  });
}



// 
function wishroundMerchantEmailFriendIsLoad() {
  $("#wishroundMerchantEmailFriendRow").addClass("is-load");
}
function wishroundMerchantEmailFriendIsNoLoad() {
  $("#wishroundMerchantEmailFriendRow").removeClass("is-load");
}

function wishroundMerchantEmailFriendOpenPass() {
  $(".js-validateform2").addClass("is-open-pass");
}

//
function wishroundMerchantEmailIsLoad() {
  $("#wishroundMerchantEmailMyRow").addClass("is-load");
}
function wishroundMerchantEmailIsNoLoad() {
  $("#wishroundMerchantEmailMyRow").removeClass("is-load");
}

function wishroundMerchantEmailOpenPass() {
  $(".js-validateform3").addClass("is-open-pass");
}

// Кросс-браузерная функция для получения символа из события keypress:
// больше инфы тут https://learn.javascript.ru/keyboard-events#getChar

function getChar(event) {
  if (event.which == null) { // IE
    if (event.keyCode < 32) return null; // спец. символ
    return String.fromCharCode(event.keyCode)
  }

  if (event.which != 0 && event.charCode != 0) { // все кроме IE
    if (event.which < 32) return null; // спец. символ
    return String.fromCharCode(event.which); // остальные
  }

  return null; // спец. символ
}



function openWishReadyFriend() {
  $("#wishroundMerchantReadyFriend").show();
  $("#one-wish").hide(); // может быть измененно. В зависимости от того какой попап открыт
}

function openWishReadyMe() {
  $("#wishroundMerchantReadyMy").show();
  $("#one-wish").hide(); // может быть измененно. В зависимости от того какой попап открыт
}


function playVideoPopup() {
  $("#checkoutHowWork").hide();
  $("#wishroundHowVideo").show();
}
function openMerchantCreate() {
  $("#wishroundMerchantCreate").show();
  $("#checkoutHowWork").hide();
}
function modifyMerchantPopup() {
  $("#popup-wrap").addClass("is-modify");
}
function resetModifyMerchantPopup() {
  $("#popup-wrap").removeClass("is-modify");
}
function modifyMerchantPopupReady() {
  $("#popup-wrap").addClass("is-modify-ready"); // убираем стрелочку назад
}
function resetModifyMerchantPopupReady() {
  $("#popup-wrap").removeClass("is-modify-ready");
}

// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
function onYouTubeIframeAPIReady() {
  player = new YT.Player('player', {
    height: '490',
    width: '820',
    videoId: '-GNhQx_DMfk',
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
  $('.js-play-video').on('click', function () {
    event.target.playVideo();
  });
}


// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event) {
  // if (event.data == YT.PlayerState.PLAYING && !done) {
  //   setTimeout(stopVideo, 6000);
  //   done = true;
  // }
}
function stopVideo() {
  player.stopVideo();
}

function removeIframe() {
  window.parent.document.getElementById("wishroundIframeOverlay").remove();
}





function hideAllPopup() { 
  var popupWishroundSocial              =    $("#wishroundSocial");
  var popupWishroundProcess             =    $("#wishroundProcess");
  var popupWishroundPayment             =    $("#popupWishroundPayment");
  var popupWishroundFaq1                =    $("#popupWishroundFaq1");
  var popupWishroundFaq2                =    $("#popupWishroundFaq2");
  var popupWishroundFaq3                =    $("#popupWishroundFaq3");
  var wishroundMerchantReadyMyNew       =    $("#wishroundMerchantReadyMyNew");

  popupWishroundSocial.hide();
  popupWishroundProcess.hide();
  popupWishroundPayment.hide();
  popupWishroundFaq1.hide();
  popupWishroundFaq2.hide();
  popupWishroundFaq3.hide();
  wishroundMerchantReadyMyNew.hide();
}

function showWishroundSocial() {
  var popupWishroundSocial = $("#wishroundSocial");

  popupWishroundSocial.show();

}
function showWishroundProcess() {
  var popupWishroundProcess = $("#wishroundProcess");

  popupWishroundProcess.show();
}
function showWishroundPayment() {
  var popupWishroundPayment = $("#popupWishroundPayment");

  popupWishroundPayment.show();
}
function showWishroundFaq1() {
  var popupWishroundFaq1 = $("#popupWishroundFaq1");

  popupWishroundFaq1.show();
}
function showWishroundFaq2() {
  var popupWishroundFaq2 = $("#popupWishroundFaq2");

  popupWishroundFaq2.show();
}
function showWishroundFaq3() {
  var popupWishroundFaq3 = $("#popupWishroundFaq3");
  
  popupWishroundFaq3.show();
}
function wishroundMerchantReadyMyNew() {
  var wishroundMerchantReadyMyNew = $("#wishroundMerchantReadyMyNew");

  wishroundMerchantReadyMyNew.show();
}



$(document).ready(function () {

	initPopupHeight();
	initClosePopup();
  initClosePopupButton();
  
  $('.md-overlay').on('click', function () {
    removeIframe();
  });



  $('.js-play-video').on('click', function () {
    playVideoPopup();
    modifyMerchantPopup();
  });
  // close YouTubeVideo
  $('.fa.fa-arrow-left').on('click', function () {
    stopVideo();
  });

  $('.js-wish-ready').on('click', function () {
    openWishReadyFriend();
  });

  

  // step2
  $('.js-popup-mail').on('click', function () {
    $(".popup-mail").slideDown(); // открыли
    $(".popup-sign-up").slideUp(); // закрыли
    $(".popup-forgot-step1").slideUp(); // закрыли
    $(".popup-forgot-step2").slideUp(); // закрыли
  });
  $('.js-sign-in').on('click', function () {
    $(".sign-in").slideDown(); // открыли
    $(".popup-sign-up").slideUp(); // закрыли
    $(".popup-mail").slideUp(); // закрыли
    $(".popup-forgot-step1").slideUp(); // закрыли
    $(".popup-forgot-step2").slideUp(); // закрыли
  });
  $('.js-popup-sign').on('click', function () {
    $(".popup-sign-up").slideDown(); // открыли
    $(".popup-mail").slideUp(); // закрыли
    $(".sign-in").slideUp(); // закрыли
    $(".popup-forgot-step1").slideUp(); // закрыли
    $(".popup-forgot-step2").slideUp(); // закрыли
  });
  $('.js-forgot-pass').on('click', function () {
    $(".popup-forgot-step1").slideDown(); // открыли
    $(".popup-sign-up").slideUp(); // закрыли
    $(".popup-mail").slideUp(); // закрыли
    $(".sign-in").slideUp(); // закрыли
    $(".popup-forgot-step2").slideUp(); // закрыли
  });

  // validate form
  $('.js-reestablish').on('click', function () {

    $('.popup-forgot-step1 .input input').each(function () {
      var input = $(this);
      var val = input.val();
      var form = $(".popup-forgot-step1");

      // validation input
      if (val != "") {
        input.addClass("is-succes");
        input.removeClass("is-error");
      } else {
        input.addClass("is-error");
        input.removeClass("is-succes");
      }

      // input animation
      if (input.hasClass("is-error")) {
        input.parent().addClass("wobble animated");

        var delay = setTimeout(function () {
          input.parent().removeClass("wobble animated");
        }, 800)
      } else {
        input.parent().removeClass("wobble animated");
      }
    });

    var input = $(".popup-forgot-step1 .input input");
    var form = $(".popup-forgot-step1");

    // validation form
    if (!$(input).hasClass("is-error")) {
      form.addClass("is-succes");
      form.removeClass("is-error");
    } else {
      form.addClass("is-error");
      form.removeClass("is-succes");
    }

    // next step
    if ($(".popup-forgot-step1").hasClass("is-succes")) {
      $(".popup-forgot-step2").slideDown(); // открыли
      $(".popup-sign-up").slideUp(); // закрыли
      $(".popup-mail").slideUp(); // закрыли
      $(".sign-in").slideUp(); // закрыли
      $(".popup-forgot-step1").slideUp(); // закрыли
    }
    return false;
  });

  // one wish
  $('.one-wish-item').on('click', function () {
    $(this).addClass('is-active').siblings().removeClass('is-active');
  });

  $('.one-wish-item').on('click', function () {
    if ($(this).hasClass("is-special")) {
      if ($(this).hasClass("is-active")) {
        $(".one-wish-hide-meta").show();
        $(".one-wish-hide .btn-success").css("display", "inline-block");
      }
    }

    if ($(this).hasClass("is-standart")) {
      if ($(this).hasClass("is-active")) {
        $(".one-wish-hide-meta").hide();
      }
    }
  });

  $('.js-one-wish').on('click', function () {
    $("#one-wish").show();
    $("#sign-up").hide();
    modifyMerchantPopupReady();
  });

  $('.js-sendform').on('click', function () {
    $("#one-wish").addClass("md-show");
    $("#sign-up").removeClass("md-show");
  });

  
  

  // datepicker
  $(function(){
    $('.js-datepicker').datepicker({
      minDate: 0,
      showOtherMonths: true,
      selectOtherMonths: false
    });
  });

  // валидация первой формы с почтой
  $( "#wishroundMerchantEmailFriend" ).keyup(function(e) {
    var myValue = this.value;
    // console.log(myValue);
    if (myValue.length > 3) {
      wishroundMerchantEmailFriendIsLoad();
    } else {
      wishroundMerchantEmailFriendIsNoLoad();
    }

    if (myValue.length > 5) {
      wishroundMerchantEmailFriendOpenPass();
    }
  });

  // валидация второй формы с почтой
  $( "#wishroundMerchantEmail" ).keyup(function(e) {
    var myValue = this.value;
    // console.log(myValue);
    if (myValue.length > 3) {
      wishroundMerchantEmailIsLoad();
    } else {
      wishroundMerchantEmailIsNoLoad();
    }

    if (myValue.length > 5) {
      wishroundMerchantEmailOpenPass();
    }
  });

	function updateText(event){
		var input=$(this);
		setTimeout(function(){
			var val=input.val();
			if(val!="")
				input.parent().addClass("floating-placeholder-float");
			else if (!$(".floating-placeholder").hasClass("m-link"))
				input.parent().removeClass("floating-placeholder-float");
		},100)
	}
	$(".floating-placeholder input, .floating-placeholder textarea").keydown(updateText);
	$(".floating-placeholder input, .floating-placeholder textarea").change(updateText);

	// switcher
	$('.js-switchoption .switchoption-item').on('click', function() {
    var item = $("#form-name-my label");

		if ($(this).attr("id") === "friend" ) {
			$(".js-validateform0").addClass("is-switch");
		}

		if ($(this).attr("id") === "for-me" ) {
			$(".js-validateform0").removeClass("is-switch");
		}
	});

  // tooltip hint
  $( ".js-tooltip-error-secure" ).hover(
    function() {
      $(".tooltip-error-secure").stop();
      $(".tooltip-error-secure").fadeIn(400);
    }, function() {
      $(".tooltip-error-secure").stop();
      $(".tooltip-error-secure").fadeOut(400);
    }
  );

  // tooltip hint
  $( ".js-tooltip-hint1" ).hover(
    function() {
      $(".tooltip-hint").stop();
      $(".tooltip-hint").fadeIn(400);
    }, function() {
      $(".tooltip-hint").stop();
      $(".tooltip-hint").fadeOut(400);
    }
  );

  // tooltip hint
  $( ".js-tooltip-hint2" ).hover(
    function() {
      $(".tooltip-hint").stop();
      $(".tooltip-hint").fadeIn(400);
    }, function() {
      $(".tooltip-hint").stop();
      $(".tooltip-hint").fadeOut(400);
    }
  );

  // validate form
  $('.js-sendform0').on('click', function () {
    $('.js-validateform0 .input input').each(function () {
      var input = $(this);
      var val = input.val();
      var form = $(".js-validateform0");

      // validation input
      if (val != "") {
        input.addClass("is-succes");
        input.removeClass("is-error");
      } else {
        input.addClass("is-error");
        input.removeClass("is-succes");
      }

      // input animation
      if (input.hasClass("is-error")) {
        input.parent().addClass("wobble animated");

        var delay = setTimeout(function () {
            input.parent().removeClass("wobble animated");
        }, 800)
      } else {
        input.parent().removeClass("wobble animated");
      }
    });

    // 
    $('.js-validateform0 .textarea textarea').each(function () {
      var textarea = $(this);
      var val = textarea.val();
      var form = $(".js-validateform0");

      // validation textarea
      if (val != "") {
        textarea.addClass("is-succes");
        textarea.removeClass("is-error");
      } else {
        textarea.addClass("is-error");
        textarea.removeClass("is-succes");
      }

      // textarea animation
      if (textarea.hasClass("is-error")) {
        textarea.parent().addClass("wobble animated");

        var delay = setTimeout(function () {
            textarea.parent().removeClass("wobble animated");
        }, 800)
      } else {
        textarea.parent().removeClass("wobble animated");
      }
    });

    var input = $(".js-validateform0 .input input");
    var textarea = $(".js-validateform0 .textarea textarea");
    var form = $(".js-validateform0");

    // validation form ( && textarea )
    if (!$(input).hasClass("is-error") && !$(textarea).hasClass("is-error")) {
      form.addClass("is-succes");
      form.removeClass("is-error");
    } else {
      form.addClass("is-error");
      form.removeClass("is-succes");
    }

    // NEXT STEP
    if ($(".js-validateform0").hasClass("is-succes")) {
      if ($(".js-validateform0.m-switch").hasClass("is-switch")) {
        console.log('xxx');
        // $("#wishroundMerchantAlmostReadyFriend").addClass("md-show");
        $("#sign-up").show();
        $("#wishroundMerchantCreate").hide();
      } else {
        console.log('vvv');
        // $("#wishroundMerchantAlmostReadyMe").addClass("md-show");
        $("#sign-up").show();
        $("#wishroundMerchantCreate").hide();
      }

    }
    return false;
  });
  // validate form
  $('.js-sendform2').on('click', function () {

    $('.js-validateform2 .input input').each(function () {
      var input = $(this);
      var val = input.val();
      var form = $(".js-validateform2");

      // need this if for validate password input
      // если у js-validateform2 появилось поле с паролем
      if (!$(".js-validateform2").hasClass("is-open-pass")) {
        // 
        if (!$(input).hasClass("is-password")) {

          if (val != "") {
            input.addClass("is-succes");
            input.removeClass("is-error");
          } else {
            input.addClass("is-error");
            input.removeClass("is-succes");
          }

        }
      }
      // если для js-validateform2 мы подтянули с БД почту юзера
      else {
        if (val != "") {
          input.addClass("is-succes");
          input.removeClass("is-error");
        } else {
          input.addClass("is-error");
          input.removeClass("is-succes");
        }
      }
      // input animation
      if (input.hasClass("is-error")) {
        input.parent().addClass("wobble animated");

        var delay = setTimeout(function () {
          input.parent().removeClass("wobble animated");
        }, 800)
      } else {
        input.parent().removeClass("wobble animated");
      }
    });

    var input = $(".js-validateform2 .input input");
    var form = $(".js-validateform2");

    // validation form
    if (!$(input).hasClass("is-error")) {
      form.addClass("is-succes");
      form.removeClass("is-error");
    } else {
      form.addClass("is-error");
      form.removeClass("is-succes");
    }

    // open
    if ($(".js-validateform2").hasClass("is-succes")) {
      $("#wishroundMerchantAlmostReadyFriend").removeClass("md-show");
      $("#wishroundMerchantReadyFriend").addClass("md-show");
    }
    return false;
  });
  // validate form
  $('.js-sendform3').on('click', function () {

    $('.js-validateform3 .input input').each(function () {
      var input = $(this);
      var val = input.val();
      var form = $(".js-validateform3");

      // 
      if (!$(".js-validateform3").hasClass("is-open-pass")) {
        // 
        if (!$(input).hasClass("is-password")) {

          // validation input
          if (val != "") {
            input.addClass("is-succes");
            input.removeClass("is-error");
          } else {
            input.addClass("is-error");
            input.removeClass("is-succes");
          }

        }
      }

      // если для js-validateform3 мы подтянули с БД почту юзера
      else {
        if (val != "") {
          input.addClass("is-succes");
          input.removeClass("is-error");
        } else {
          input.addClass("is-error");
          input.removeClass("is-succes");
        }
      }

      // input animation
      if (input.hasClass("is-error")) {
        input.parent().addClass("wobble animated");

        var delay = setTimeout(function () {
          input.parent().removeClass("wobble animated");
        }, 800)
      } else {
        input.parent().removeClass("wobble animated");
      }
    });

    var input = $(".js-validateform3 .input input");
    var form = $(".js-validateform3");

    // validation form
    if (!$(input).hasClass("is-error")) {
      form.addClass("is-succes");
      form.removeClass("is-error");
    } else {
      form.addClass("is-error");
      form.removeClass("is-succes");
    }

    // open
    if ($(".js-validateform3").hasClass("is-succes")) {
      $("#wishroundMerchantAlmostReadyMe").removeClass("md-show");
      $("#wishroundMerchantReadyMy").addClass("md-show");
    }
    return false;
  });

  // validate form
  $('.js-sendform4').on('click', function () {

    $('.js-validateform4 .textarea textarea').each(function () {
      var input = $(this);
      var val = input.val();
      var form = $(".js-validateform4");

      // validation input
      if (val != "") {
        input.addClass("is-succes");
        input.removeClass("is-error");
      } else {
        input.addClass("is-error");
        input.removeClass("is-succes");
      }

      // input animation
      if (input.hasClass("is-error")) {
        input.parent().addClass("wobble animated");

        var delay = setTimeout(function () {
          input.parent().removeClass("wobble animated");
        }, 800)
      } else {
        input.parent().removeClass("wobble animated");
      }
    });

    var input = $(".js-validateform4 .textarea textarea");
    var form = $(".js-validateform4");

    // validation form
    if (!$(input).hasClass("is-error")) {
      form.addClass("is-succes");
      form.removeClass("is-error");
    } else {
      form.addClass("is-error");
      form.removeClass("is-succes");
    }

    // open
    if ($(".js-validateform4").hasClass("is-succes")) {
      $("#wishroundMerchantAlmostReadyFriend").hide(); // ?
      $("#wishroundMerchantSend").hide();
      $("#wishroundMerchantLettrsSend").show();
    }
    return false;
  });

  $('.js-merchant-create').on('click', function () {
    openMerchantCreate();
    modifyMerchantPopup();
  });

  $('.js-open-checkoutHowWork').on('click', function() {
    $("#wishroundMerchantCreate").removeClass("md-show");
    $("#checkoutHowWork").addClass("md-show");
  });

  $('.js-open-wishroundMerchantCreate').on('click', function() {
    $("#wishroundMerchantAlmostReadyMe").removeClass("md-show");
    $("#wishroundMerchantAlmostReadyFriend").removeClass("md-show");
    $("#wishroundMerchantCreate").addClass("md-show");

    // $("#wishroundMerchantEmailMyRow").removeClass("is-load");
  });

  $('.wishroundMerchantReadyEdit').on('click', function() {
    $("#wishroundMerchantReadyMy").removeClass("md-show");
    $("#wishroundMerchantReadyFriend").removeClass("md-show");
    $("#wishroundMerchantCreate").addClass("md-show");
  });

  $('#btn-share-on-facebook').on('click', function() {
    $("#wishroundMerchantReadyMy").hide();
    $("#wishroundMerchantWishPosted").show();
    resetModifyMerchantPopupReady();
  });
  
  $('.wishroundMerchantMail').on('click', function() {
    $("#wishroundMerchantReadyFriend").hide();
    $("#wishroundMerchantSend").show();
    resetModifyMerchantPopupReady();
  });

  $('.js-open-ready-friend').on('click', function() {
    $("#wishroundMerchantSend").removeClass("md-show");
    $("#wishroundMerchantReadyFriend").addClass("md-show");
  });

  //
  $('.js-send-mail').on('click', function() {
    $("#wishroundMerchantReadyFriend").removeClass("md-show");
    $("#wishroundMerchantSend").addClass("md-show");
  });
  
  $('.js-send-mail-my').on('click', function() {
    $("#wishroundMerchantReadyMy").removeClass("md-show");
    $("#wishroundMerchantSend").addClass("md-show");

    $('.js-open-ready-friend').on('click', function() {
      $("#wishroundMerchantReadyMy").addClass("md-show");
      $("#wishroundMerchantSend").removeClass("md-show");
    });
  });
  
  // one wish
  $('.one-wish-item').on('click', function () {
    $(this).addClass('is-active').siblings().removeClass('is-active');
  });

  // faq
  $('.js-faq-list li').on('click', function() {
    if ($(this).hasClass("is-active")) {
      $(this).removeClass("is-active");
    } else {
      $(this).addClass('is-active').siblings().removeClass('is-active');
    }
  });

});

