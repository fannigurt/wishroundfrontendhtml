$( document ).ready(function() {

	/*!
	 * jQuery.ellipsis
	 * http://github.com/jjenzz/jquery.ellipsis
	 * --------------------------------------------------------------------------
	 * Copyright (c) 2013 J. Smith (@jjenzz)
	 * Dual licensed under the MIT and GPL licenses: 
	 * http://www.opensource.org/licenses/mit-license.php
	 * http://www.gnu.org/licenses/gpl.html
	 *
	 * adds a class to the last 'allowed' line of text so you can apply
	 * text-overflow: ellipsis;
	 */
	(function(a){if(typeof define==="function"&&define.amd){define(["jquery"],a)}else{a(jQuery)}}(function(d){var c="ellipsis",b='<span style="white-space: nowrap;">',e={lines:"auto",ellipClass:"ellip",responsive:false};function a(h,q){var m=this,w=0,g=[],k,p,i,f,j,n,s;m.$cont=d(h);m.opts=d.extend({},e,q);function o(){m.text=m.$cont.text();m.opts.ellipLineClass=m.opts.ellipClass+"-line";m.$el=d('<span class="'+m.opts.ellipClass+'" />');m.$el.text(m.text);m.$cont.empty().append(m.$el);t()}function t(){if(typeof m.opts.lines==="number"&&m.opts.lines<2){m.$el.addClass(m.opts.ellipLineClass);return}n=m.$cont.height();if(m.opts.lines==="auto"&&m.$el.prop("scrollHeight")<=n){return}if(!k){return}s=d.trim(m.text).split(/\s+/);m.$el.html(b+s.join("</span> "+b)+"</span>");m.$el.find("span").each(k);if(p!=null){u(p)}}function u(x){s[x]='<span class="'+m.opts.ellipLineClass+'">'+s[x];s.push("</span>");m.$el.html(s.join(" "))}if(m.opts.lines==="auto"){var r=function(y,A){var x=d(A),z=x.position().top;j=j||x.height();if(z===f){g[w].push(x)}else{f=z;w+=1;g[w]=[x]}if(z+j>n){p=y-g[w-1].length;return false}};k=r}if(typeof m.opts.lines==="number"&&m.opts.lines>1){var l=function(y,A){var x=d(A),z=x.position().top;if(z!==f){f=z;w+=1}if(w===m.opts.lines){p=y;return false}};k=l}if(m.opts.responsive){var v=function(){g=[];w=0;f=null;p=null;m.$el.html(m.text);clearTimeout(i);i=setTimeout(t,100)};d(window).on("resize."+c,v)}o()}d.fn[c]=function(f){return this.each(function(){try{d(this).data(c,(new a(this,f)))}catch(g){if(window.console){console.error(c+": "+g)}}})}}));

	// text ellipsis
	$('.js-one-lines').ellipsis({
		lines: 1
	});
	$('.js-two-lines').ellipsis({
		lines: 2
	});
	$('.js-tree-lines').ellipsis({
		lines: 3
	});

	// function scrollIndex() {
	// 	if ($(window).width() > 980) {
	// 		smoothScroll.init({
	// 			offset: 60
	// 		});
	// 	} else {
	// 		smoothScroll.init({
	// 			offset: 0
	// 		});
	// 	}
	// }
	// scrollIndex();


	// function indexSlide() {
	// 	var windowHeight = $(window).height();
	// 	var headerHeight = $("header").height();
	// 	var slideHeight = windowHeight - headerHeight;

	// 	if ($(window).height() > 380) {
	// 		// set height for slide on index page
	// 		$(".slider .slider-item").css("height", slideHeight);
	// 		$(".slider .table").css("height", slideHeight);
	// 	}
	// } 
	// if ($(".slider").length > 0){
	// 	indexSlide();
	// }

	// неактуально, убрали
	// function manual1() {
	// 	var windowHeight = $(window).height();

	// 	if ($(window).width() > 980) {
	// 		$(".section-full").css("min-height", windowHeight);
	// 	}
	// }
	// if ($(".section-full").length > 0){
	// 	manual1();
	// }
	
	// function slick() {
	// 	$('.js-slick').slick({
	// 		dots: true,
	// 		arrows: false,
	// 		infinite: true,
	// 		slidesToShow: 1,
	// 		autoplay: true,
	// 		autoplaySpeed: 5000
	// 	});
	// } 
	// if ($(".js-slick").length > 0){
	// 	slick();
	// }
	
	// function slick2() {
	// 	$('.js-shop').slick({
	// 		dots: false,
	// 		infinite: true,
	// 		slidesToShow: 4,
	// 		responsive: [
	// 		{
	// 			breakpoint: 1150,
	// 			settings: {
	// 				slidesToShow: 3,
	// 				slidesToScroll: 1
	// 			}
	// 		},
	// 		{
	// 			breakpoint: 800,
	// 			settings: {
	// 				slidesToShow: 2,
	// 				slidesToScroll: 1
	// 			}
	// 		},
	// 		{
	// 			breakpoint: 580,
	// 			settings: {
	// 				slidesToShow: 1,
	// 				slidesToScroll: 1
	// 			}
	// 		}
	// 		]
	// 	});
	// } 
	// if ($(".js-shop").length > 0){
	// 	slick2();
	// }

	// function slick3() {
	// 	$('.js-prod').slick({
	// 		dots: false,
	// 		infinite: true,
	// 		slidesToShow: 3,
	// 		responsive: [
	// 			{
	// 				breakpoint: 960,
	// 				settings: {
	// 					slidesToShow: 2,
	// 					slidesToScroll: 1
	// 				}
	// 			},
	// 			{
	// 				breakpoint: 700,
	// 				settings: {
	// 					slidesToShow: 1,
	// 					slidesToScroll: 1
	// 				}
	// 			},
	// 		]
	// 	});
	// } 
	// if ($(".js-prod").length > 0){
	// 	slick3();
	// }

	// function slick4() {
	// 	$('.js-user').slick({
	// 		dots: true,
	// 		infinite: true,
	// 		arrows: false,
	// 		slidesToShow: 2,
	// 		slidesToScroll: 2,
	// 		responsive: [
	// 			{
	// 				breakpoint: 960,
	// 				settings: {
	// 					slidesToShow: 2,
	// 					slidesToScroll: 2
	// 				}
	// 			},
	// 			{
	// 				breakpoint: 768,
	// 				settings: {
	// 					slidesToShow: 1,
	// 					slidesToScroll: 1
	// 				}
	// 			},
	// 		]
	// 	});
	// } 
	// if ($(".js-user").length > 0){
	// 	slick4();
	// }

	// mob device hide overlay there is active nav
	if ($(window).width() < 960) {
		$('.nav-list a').on('click', function() {
			$("body").removeClass("is-overlay");
			$(".nav-list").slideUp();
		});
	}

	$('.js-nav-index a').on('click', function() {
	 $(".js-nav-mobile").removeClass("is-active");
	});

	$('.js-nav-mobile').on('click', function() {
		$(".nav-list").slideToggle();
		$("body").toggleClass("is-overlay");
		$(this).toggleClass("is-active");
	});

	$('.nav-overlay').on('click', function() {
		$(".nav-list").slideUp();
		$("body").removeClass("is-overlay");
		$(".js-nav-mobile").removeClass("is-active");
	});

	function lang() {
		$('.lang .lang-select span').on('click', function() {
			if ($(".lang").hasClass("is-open")) {
				$(".lang").removeClass("is-open");
			}
			else {
				$(".lang").addClass("is-open");
				$(".lang-overlay").addClass("is-active");

				$('.lang-subnav li span').on('click', function() {
					var activeLang = $(this).text();
					$(".lang-select > span").text(activeLang);

					// active class for select lang
					$(this).parent().addClass('is-active').siblings().removeClass('is-active');

					// remove overlay else active nav
					$(".lang-overlay").removeClass("is-active");
				});
			}
		});
		if ($(window).width() < 481) {
			$(".lang-subnav .hide480").remove();
		}
	}
	if ($(".lang").length > 0){
		lang();
	}

	// lang mobile
	$('.m-mobile-lang span').on('click', function() {
		$(".m-mobile-lang ul").slideToggle();
		
		$('.m-mobile-lang ul div').on('click', function() {
			var activeLang = $(this).text();
			$(this).parent().addClass('is-active').siblings().removeClass('is-active');

			$('.m-mobile-lang span').text(activeLang);
		});
	});

	$('.lang-overlay').on('click', function() {
		if ($(".lang").hasClass("is-open")) {
			$(".lang").removeClass("is-open");
		}
		if ($('.lang-overlay').hasClass("is-active")) {
			$(".lang-overlay").removeClass("is-active");
		}
	});

	$('.js-mobile-enter').on('click', function() {
		if ($(window).width() < 481) {
			if ($(".nav-mobile").hasClass) {
				$(".nav-mobile").removeClass("is-active");
			}
		}
	});

	function animateIndex() {

		if ($(window).width() > 980) {
			// #about-us animate
			var position = $("#about-us").height() / 2 + 50;

			if ($(window).scrollTop() > position) {
				$("#how-work").addClass('is-animate');

			} else {
				$("#how-work").removeClass('is-animate');
			}

			// #partners animate
			var position = $(".quote").offset().top + 300;

			if ($(window).scrollTop() > position) {
				$("#partners").addClass('is-animate');

			} else {
				$("#partners").removeClass('is-animate');
			}
		}
	}
	if ($(".quote").length > 0){
		animateIndex();
	}

	(function() {
		[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {	
			new SelectFx(el);
		} );
	})();

	function updateText(event){
		var input=$(this);
		setTimeout(function(){
			var val=input.val();
			if(val!="")
				input.parent().addClass("floating-placeholder-float");
			else if (!$(".floating-placeholder").hasClass("m-link"))
				input.parent().removeClass("floating-placeholder-float");
			// else
			// 	input.parent().addClass("floating-placeholder-float");
		},100)
	}
	$(".floating-placeholder input, .floating-placeholder textarea").keydown(updateText);
	$(".floating-placeholder input, .floating-placeholder textarea").change(updateText);

	// datepicker
	$(function(){
		$('.js-datepicker').datepicker({
			minDate: 0,
			showOtherMonths: true,
			selectOtherMonths: false
		});
	});

	$(function() {
		$('.cookie-succes').on('click', function() {
			$(".cookie").slideUp();
			return false;
		});
	});

	// popup
	function popup() {
		var bodyHeight = $("body").height();
		$(".md-modal .md-overlay").css("min-height", bodyHeight);
		// console.log(bodyHeight);

		$('.md-trigger').on('click', function() {
			$("body").addClass("is-popup");
			$("html").addClass("is-popup");

			if ($(window).width() < 960) {
				$("body").removeClass("is-overlay");
				$(".nav-list").slideUp();

				$('.md-modal .md-content').each(function() {
					var heightPopupContent = $(this).outerHeight();
					var heightPopup = heightPopupContent + 60
					var minheightOverlay = $(this).parent().find(".md-overlay").css("min-height", heightPopup);
				});
			}
		});
		$('.md-close').on('click', function() {
			$("body").removeClass("is-popup");
			$("html").removeClass("is-popup");
			$(".md-modal").removeClass("md-show");
			$(".nav-mobile").removeClass("is-active");
		});
		$('.md-overlay').on('click', function() {
			$(".md-modal").removeClass("md-show");
			$("body").removeClass("is-popup");
			$("html").removeClass("is-popup");
			$(".nav-mobile").removeClass("is-active");
		});
	}
	if ($(".md-modal").length > 0){
		popup();
	}

	// // step2
	// $('.js-popup-mail').on('click', function() {
	// 	$(".popup-mail").slideDown(); // открыли
	// 	$(".popup-sign-up").slideUp(); // закрыли
	// 	$(".popup-forgot-step1").slideUp(); // закрыли
	// 	$(".popup-forgot-step2").slideUp(); // закрыли
	// });
	// $('.js-sign-in').on('click', function() {
	// 	$(".sign-in").slideDown(); // открыли
	// 	$(".popup-sign-up").slideUp(); // закрыли
	// 	$(".popup-mail").slideUp(); // закрыли
	// 	$(".popup-forgot-step1").slideUp(); // закрыли
	// 	$(".popup-forgot-step2").slideUp(); // закрыли
	// });
	// $('.js-popup-sign').on('click', function() {
	// 	$(".popup-sign-up").slideDown(); // открыли
	// 	$(".popup-mail").slideUp(); // закрыли
	// 	$(".sign-in").slideUp(); // закрыли
	// 	$(".popup-forgot-step1").slideUp(); // закрыли
	// 	$(".popup-forgot-step2").slideUp(); // закрыли
	// });
	// $('.js-forgot-pass').on('click', function() {
	// 	$(".popup-forgot-step1").slideDown(); // открыли
	// 	$(".popup-sign-up").slideUp(); // закрыли
	// 	$(".popup-mail").slideUp(); // закрыли
	// 	$(".sign-in").slideUp(); // закрыли
	// 	$(".popup-forgot-step2").slideUp(); // закрыли
	// });
	// $('.js-reestablish').on('click', function() {
	// 	$(".popup-forgot-step2").slideDown(); // открыли
	// 	$(".popup-sign-up").slideUp(); // закрыли
	// 	$(".popup-mail").slideUp(); // закрыли
	// 	$(".sign-in").slideUp(); // закрыли
	// 	$(".popup-forgot-step1").slideUp(); // закрыли
	// });

	// // one wish
	// $('.one-wish-item').on('click', function() {
	//  $(this).addClass('is-active').siblings().removeClass('is-active');
	// });

	// // form first active
	// $(".form .cs-options li").first().addClass("cs-selected");

	
	// // tooltip
	// $( ".js-tooltip-error" ).hover(
	// 	function() {
	// 		$(".tooltip-error").fadeIn();
	// 	}, function() {
	// 	 $(".tooltip-error").fadeOut();
	// 	}
	// );
	// if ($(".tooltip-error").hasClass("is-no-valid")) {
	// 	$(".tooltip-error").fadeIn().delay(2800).fadeOut();
	// }

	// // table check
	// $('.import-table .import-table-item').on('click', function() {
	// 	$(this).toggleClass("is-active");
	// 	// TOOGLE CHECK
	// });

	// REGULAR (pattern)

	// http://javascript.ru/basic/regular-expression+#strokovye-metody-poisk-i-zamena
	// http://code.tutsplus.com/tutorials/8-regular-expressions-you-should-know--net-6149
	// http://habrahabr.ru/post/123845/

	// number
	$(".pt-num").keypress(function (e) {
		var reg = /[^0-9]/g
		if (String.fromCharCode(e.keyCode).match(reg)) return false;
	});

	// user
	$(".pt-user").keypress(function (e) {
		var reg = /[^a-zа-яё0-9-_\.]/gi
		if (String.fromCharCode(e.keyCode).match(reg)) return false;
	});

	// preurl
	$(".pt-preurl").keypress(function (e) {
		var reg = /[^a-zA-Z0-9]/gi
		if (String.fromCharCode(e.keyCode).match(reg)) return false;
	});

	// manual 3
	// $('.js-send-mail').on('click', function() {
	// 	$(".share").hide();
	// 	$(".mail-step1").show();
	// });

	// $('.js-import').on('click', function() {
	//  $(".mail-step1").hide();
	//  $(".import").show();
	// });

	// validate form
	$('.js-sendform').on('click', function() {

		$('.js-validateform .input input').each(function() {
			var input = $(this);
			var val = input.val();
			var form = $(".js-validateform");

			// validation input
			if (val!="") {
				input.addClass("is-succes");
				input.removeClass("is-error");
			} else {
				input.addClass("is-error");
				input.removeClass("is-succes");
			}

			// input animation
			if (input.hasClass("is-error")) {
				input.parent().addClass("wobble animated");

				var delay = setTimeout(function(){
					input.parent().removeClass("wobble animated");
				}, 800)
			} else {
				input.parent().removeClass("wobble animated");
			}

			// validation form
			if (!$(input).hasClass("is-error")) {
				form.addClass("is-succes");
				form.removeClass("is-error");
			} else {
				form.addClass("is-error");
				form.removeClass("is-succes");
			}
		});
		return false;
	});



	$(window).resize(function() {
		if ($(".slider").length > 0){
			indexSlide();
		}
	});

	$(window).scroll(function(){
		if ($(".quote").length > 0){
			animateIndex();
		}
		scrollIndex();
	});
	
});

// selected  manaul3
function selectedInput() {
	document.getElementById("select-input").select();
}


